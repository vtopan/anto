"""
AnTo logging APIs.

Author: Vlad Topan (vtopan/gmail)
"""

import sys
import traceback


CFG = {
    'log': lambda x:sys.stderr.write(f'[*] {x}\n'),
    'err': lambda x:sys.stderr.write(f'[!] {x}\n'),
    'warn': lambda x:sys.stderr.write(f'[~] {x}\n'),
    'dbg': lambda x:sys.stderr.write(f'[#] {x}\n'),
    'debug_level': 0,
    }


def log(s):
    """
    Output a string.
    """
    CFG['log'](s)


def err(s):
    """
    Output an error message.
    """
    CFG['err'](s)


def exc(s):
    """
    Output an error message + exception traceback.
    """
    CFG['err'](s + '\n' + traceback.format_exc(3))


def warn(s):
    """
    Output a warning message.
    """
    CFG['warn'](s)


def dbg(s, level=1):
    """
    Output a debug message.
    """
    if CFG['debug_level'] >= level:
        CFG['dbg'](s)
