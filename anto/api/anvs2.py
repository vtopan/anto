"""
AnTo-libvstruct2 integration.

Author: Vlad Topan (vtopan/gmail)
"""

from .anplugin import AnParser
from ..core import AnObject

import libvstruct2 as vs2

import os


VS2_FIELDS = ('name', 'offset', 'size', 'description')


def vs2_to_anto(vsobj, aobj):
    """
    Imports the fields (recursively) from a libvstruct2 VStruct into an AnTo AnStruct.
    """
    for f in vsobj.fields:
        args = {k:getattr(f, k, None) for k in VS2_FIELDS}
        if type(f) is list:
            for i, e in enumerate(f):
                eargs = {k:getattr(e, k, None) for k in VS2_FIELDS}
                if isinstance(e, vs2.Field):
                    aobj.add_field(value=e.formatted_value(), is_list=e.is_array, **eargs)
                else:
                    name = eargs.pop('name')
                    new_aobj = AnObject(data=aobj.data, parent=aobj, obj_type=e.__class__.__name__,
                            name=f'{name}[{i}]', **eargs)
                    # setattr(aobj, e.name, new_aobj)  # fixme - make aobj indexable
                    vs2_to_anto(e, new_aobj)
        elif isinstance(f, vs2.Field):
            if not f.flags & vs2.FLG_HIDDEN:
                aobj.add_field(value=str(f.formatted_value()), is_list=f.is_array, **args)
        else:
            new_aobj = AnObject(data=aobj.data, parent=aobj, obj_type=f.__class__.__name__, **args)
            setattr(aobj, f.name, new_aobj)
            vs2_to_anto(f, new_aobj)


def make_anparser_from_vs2(vsobj, anparser=None):
    """
    Create an AnParser from a libvstruct2 top-level parser.
    """
    def parse_generator(sobj):
        """
        Parse function generator.
        """
        def parse(self):
            vs2o = vsobj(name=os.path.basename(self.filename), dataview=self.data)
            vs2_to_anto(vs2o, self)
            self.vs2 = vs2o
            setattr(self.data, self.__class__.__name__, self)
        return parse
    name = vsobj.__name__
    if anparser is None:
        anparser = type(name, (AnParser,), {"parse": parse_generator(vsobj), 'FILTER': getattr(vsobj, 'FILTER', None)})
    else:
        anparser.preprocess = parse_generator(vsobj)
        if hasattr(vsobj, 'FILTER'):
            anparser.FILTER = vsobj.FILTER
    return anparser
