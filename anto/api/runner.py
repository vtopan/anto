"""
Process running module.

Author: Vlad Topan (vtopan/gmail)
"""

from .anlog import log, err, exc
from anto.api.lib.net import send_packed_data, recv_packed_data, recvall, recv_int, send_int

from iolib import AttrDict

import base64
import hashlib
import json
import os
import platform
import shlex
import socket
import struct
import subprocess
import tempfile
import time
import traceback
import zlib


RUN_TIMEOUT = 10 * 60 * 60
RCE_HOSTS = {}
AUTO_RUN_REMOTELY = False
AVAILABLE = {}
CHECK_CMD = {
    'binwalk': (['binwalk', '-h'], b'Usage: binwalk', -1),
    'file': (['file', '-v'], b'file-', 0),
    'tcpdump': (['tcpdump', '--help'], b'tcpdump version', 0),
}
RCE_PROTO_ERR = {
    b'i': 'Invalid sig check',
    b'e': 'Invalid / unknown command',
}
MAX_REMOTE_RUNNER_SIZE = 20 * 1024 * 1024


def check_availability(command, pattern=None, retcode=0):
    """
    Check if the given command is available in the local (or remote) shell.

    :param command: Shell command (without args).
    """
    if isinstance(command, str):
        if command in CHECK_CMD:
            command, pattern, retcode = CHECK_CMD[command]
        else:
            command = command.split(' ')
    # '' means "retry remotely"
    if AVAILABLE.get(command[0]) in ('', None):
        try:
            p = subprocess.run(command, capture_output=True)
            available = (retcode < 0 or p.returncode == retcode) and (not pattern or pattern in p.stdout + p.stderr)
        except FileNotFoundError:
            log(f'Command {command[0]} not available locally')
            available = False
        if not available and AUTO_RUN_REMOTELY:
            available = ''
            p = run_remotely(command, quiet=True)
            if (retcode < 0 or p.returncode == retcode) and (not pattern or pattern in p.stdout + p.stderr):
                available = 'remotely'
        AVAILABLE[command[0]] = available
    return AVAILABLE[command[0]]


def add_rce_host(host, port, key, name='<default>'):
    """
    Add a host.
    """
    rh = AttrDict(host=host, port=port, key=key)
    RCE_HOSTS[name] = rh


def sign_message(msg, key):
    """
    Sign a message.
    """
    if isinstance(key, str):
        key = key.encode()
    sig = hashlib.sha512(msg + key).digest()
    return sig


def gather_remote_files(filename, rce_host=True, remove=True):
    """
    Retrieve files from the remote endpoint (optionally with a wildcard) as ZIP.
    """
    rce_host = list(RCE_HOSTS.values())[-1] if rce_host is True else RCE_HOSTS[rce_host]
    data = b''
    with socket.socket(socket.AF_INET6 if ':' in rce_host.host else socket.AF_INET,
            socket.SOCK_STREAM) as s:
        s.connect((rce_host.host, rce_host.port))
        filename = filename.encode()
        sig = sign_message(filename, rce_host.key)
        s.send(b'F' if remove else b'f')
        send_packed_data(s, sig, filename)
        res = s.recv(1)
        if res == b'k':
            data = recv_packed_data(s)
        else:
            err(f'RCE protocol error: {res} / {RCE_PROTO_ERR.get(res)}!')
            data = None
        s.close()
    return data


def run_remotely(command, input=None, rce_host=True, arg_file=None, quiet=False):
    """
    Run command on a remote host.

    :param upload_file: Filename (present in command) or bytes.
    :param quiet: Don't complain if an RCE host is not defined.
    """
    p = AttrDict(returncode=-1, stdout=b'', stderr=b'')
    try:
        rce_host = list(RCE_HOSTS.values())[-1] if rce_host is True else RCE_HOSTS[rce_host]
    except IndexError:
        if not quiet:
            err('No RCE host defined!')
        p.returncode = -1000
        return p
    try:
        command = json.dumps(command).encode()
        input = input or b''
        filename = filedata = b''
        if isinstance(arg_file, bytes):
            filedata = arg_file
        elif arg_file:
            filename = arg_file.encode()
            if (size := os.path.getsize(filename)) >= MAX_REMOTE_RUNNER_SIZE:
                # todo: configurable limit
                raise ValueError(f'File {filename} is too large for a remote run argument ({size / (1 << 20):.1f}MB > {MAX_REMOTE_RUNNER_SIZE // (1 << 20)}MB)!')
            filedata = open(filename, 'rb').read()
        sig = sign_message(command + input + filename, rce_host.key)
        with socket.socket(socket.AF_INET6 if ':' in rce_host.host else socket.AF_INET,
                socket.SOCK_STREAM) as s:
            s.connect((rce_host.host, rce_host.port))
            s.send(b'x')
            send_packed_data(s, sig, command, input, filename, filedata)
            res = s.recv(1)
            if res != b'k':
                err(f'RCE protocol error: {res} / {RCE_PROTO_ERR.get(res)}!')
            else:
                p.returncode = recv_int(s)
                p.local_fn, p.stdout, p.stderr = recv_packed_data(s, count=3)
                p.local_fn = p.local_fn.decode()
    except Exception as e:
        exc(f'Failed running command {command} remotely: {e}')
    return p


def run(command, input=None, shell=None, capture_output=True, timeout=RUN_TIMEOUT, log_fail=True,
    remotely=False, arg_file=None, **kwargs):
    """
    Run command, read output.

    :param remotely: Run remotely - can be a string (key in RCE_KEYS) or True.
    :param arg_file: Passed to run_remotely (if used).
    :return: A subiprocess.CompletedProcess instance.
    """
    if remotely:
        return run_remotely(command, input=input, rce_host=remotely, arg_file=arg_file)
    if shell is None:
        shell = True if platform.system() == 'Windows' else False
    p = subprocess.run(command, input=input, shell=shell, capture_output=capture_output,
            timeout=timeout, **kwargs)
    if log_fail and p.returncode != 0:
        if type(command) in (list, tuple):
            command = shlex.join(command)
        err(f'Command [{command}] failed (status: {p.returncode})')
        if p.stderr:
            err(f'STDERR:\n{p.stderr}')
        elif p.stdout:
            err(f'STDOUT:\n{p.stdout}')
    return p
