"""
AnTo misc APIs.

Author: Vlad Topan (vtopan/gmail)
"""

from .lib.cparser import parse_c_source, C_TYPE_MAP
from .anlog import log, err, warn, dbg

import ast
import re
import struct


try:
    from libvstruct2 import PY_TO_VSTRUCT
except ImportError:
    err('Failed importing libvstruct2!')
    PY_TO_VSTRUCT = {
        'B': 'B',
        'H': 'I2',
        'I': 'I4',
        'Q': 'I8',
        'b': 'SI1',
        'h': 'SI2',
        'i': 'SI4',
        'q': 'SI8',
        }


RX = {
    'sizedefs':(r'// sizeof\((\w+)\)\s*=+\s*([\da-fx]+)', re.I),     # custom sizedefs
    }
for k in RX:
    RX[k] = re.compile(*RX[k])


def parse_c_to_anstruct(data):
    """
    Parse C source and generate AnStructs.

    :param info: debug message output function.
    """
    res = []
    constants, structs, unions = parse_c_source(data)
    known_size_map = {m[0]:ast.literal_eval(m[1]) for m in RX['sizedefs'].findall(data)}
    known_sizes = set(x.lower() for x in known_size_map)
    known_types = set(C_TYPE_MAP)
    constant_map = {}
    for name, (value, comment) in sorted(constants.items()):
        line = f'{name} = {value}'
        if comment:
            line = f'{line:60s} # {comment}'
        try:
            constant_map[name] = ast.literal_eval(value)
        except (SyntaxError, ValueError):
            pass
        res.append(line)
    res.append('\n')
    for name, fields in structs.items():
        log(f'Structure: {name}')
        field_types = set(e[1].lower() for e in fields)
        log(f'  - field types: {", ".join(sorted(field_types))}')
        if not (field_types <= known_types):
            diff = field_types - known_types
            err(f'  - unknown field types: {", ".join(diff)}')
            if diff - known_sizes:
                warn(f'  - please add `// sizeof(...)=...` comments for the following fields: {", ".join(diff - known_sizes)}')
                continue
        fmt_lst = []
        for f in fields:
            fmt = C_TYPE_MAP.get(f[1].lower())
            if f[2]:
                # count > 1
                size = struct.calcsize(fmt) if fmt else known_size_map.get(f[1])
                try:
                    size *= constant_map[f[2]] if f[2] in constant_map else ast.literal_eval(f[2])
                except (SyntaxError, ValueError):
                    err(f'Failed evaluating {f[2]} - please define it in the source!')
                    return
                fmt_lst.append(f'{size}s')
            else:
                fmt_lst.append(fmt or f'{known_size_map.get(f[1])}s')
        sformat = '<' + ''.join(fmt_lst)
        dbg('  - struct format: ' + sformat)
        res += [f'class {name}(AnCStructParser):']
        res.append('    FIELD_NAMES = [%s]' % ', '.join(f'"{e[0]}"' for e in fields))
        res.append(f'    STRUCT_FORMAT = "{sformat}"\n\n')
    return '\n'.join(res) + '\n'


def parse_c_to_vstruct(data):
    """
    Parse C source and generate VStructs.

    :param info: debug message output function.
    """
    res = []
    constants, structs, unions = parse_c_source(data)
    constant_map = {}
    for name, (value, comment) in sorted(constants.items()):
        line = f'{name} = {value}'
        if comment:
            line = f'{line:60s} # {comment}'
        try:
            constant_map[name] = ast.literal_eval(value)
        except (SyntaxError, ValueError):
            pass
        res.append(line)
    res.append('\n')
    for name, fields in structs.items():
        res.append(f'{name}:')
        for f in fields:
            fmt = PY_TO_VSTRUCT.get(C_TYPE_MAP.get(f[1].lower(), f[1]), f[1])
            res.append(f'  - {f[0]}: {fmt}')
        res.append('')
    return '\n'.join(res) + '\n'


def make_internal_link(text, buffer_id, offset=None, count=None, link_op='jump', filename=None):
    """
    Generate an internal link (e.g. jump to offset).
    """
    if link_op != 'jump':
        raise ValueError(f'Unknown internal link scheme {link_op}!')
    res = f'<a href="anto://jump/{buffer_id}/{offset}/{count}">{text}</a>'
    return res
