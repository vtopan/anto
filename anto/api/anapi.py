"""
AnApi: AnTo API.

Author: Vlad Topan (vtopan/gmail)
"""
from ..core import AnObjMgr, AnObject
from .config import init_config, CFG
from .anplugin import AnParser, AnHookParser, AnInfo, AnDataOp
from .anlog import CFG as LOG_CFG
from .anvs2 import make_anparser_from_vs2
from .anmisc import make_internal_link
from . import lib

import iolib
import libvstruct2 as vs2

import glob
from importlib.util import spec_from_file_location, module_from_spec
import os
import traceback


class AnApi:

    def __init__(self, main_window, plugin_paths=None, init_cfg=True, load_plugins=True):
        if init_cfg:
            init_config()
        self.CFG = CFG
        self.win = main_window
        self.win.api = self
        self.win.apply_config()
        self.objmgr = AnObjMgr(api=self)
        self.plugins = {'parser':[], 'hooks':{}, 'dataop':[], 'info':[]}
        self.plugins_by_name = {}
        self.parsers = self.plugins['parser']
        self.hooks = self.plugins['hooks']
        self.dataops = {}
        self.anto_path = os.path.abspath(os.path.dirname(__file__) + '/..')
        self.plugin_paths = plugin_paths or [f'{self.anto_path}/plugins']
        self.lib = lib
        self.iolib = iolib
        self.loading_file = False
        self.tainted_info = set()
        self.info = {}
        for library in (lib,):
            for k, v in library.__dict__.items():
                if k[0] != '_':
                    setattr(self, k, v)
        for e in ('dbg', 'log', 'warn', 'err'):
            LOG_CFG[e] = getattr(self, e)
        if load_plugins:
            self.load_plugins()


    def log(self, msg):
        """
        Logs a message (and shows it in the UI).
        """
        self.win.log(msg)


    def dbg(self, msg):
        """
        Logs a debug message (if enabled).
        """
        self.win.log('[DEBUG] ' + msg)


    def warn(self, msg):
        """
        Logs a warning message.
        """
        self.win.log('[WARNING] ' + msg)


    def err(self, msg):
        """
        Logs an error message.
        """
        self.win.log('[ERROR] ' + msg)


    def load_plugins(self):
        """
        Load plugins.
        """
        for path in self.plugin_paths:
            for ptype, pclass in [
                    ('parser', AnParser),
                    ('dataop', AnDataOp),
                ]:
                for f in glob.glob(f'{path}/{ptype}/*.py'):
                    name = os.path.basename(f).rsplit('.', 1)[0]
                    spec = spec_from_file_location(name, f)
                    mod = module_from_spec(spec)
                    spec.loader.exec_module(mod)
                    for varname, var in mod.__dict__.items():
                        try:
                            if not (varname[0] != '_' and issubclass(var, pclass) and var not in (AnInfo, AnParser, AnDataOp)):
                                continue
                        except TypeError:
                            continue
                        self.add_plugin(ptype, var)
        # libvstruct2-based parsers
        for p in vs2.ROOT_PARSERS:
            # the vs2 parser can extend a native AnTo parser if one exists or can be fully standalone
            native_parser = self.plugins_by_name.get(p.__name__)
            vs2_parser = make_anparser_from_vs2(p, native_parser)
            if vs2_parser != native_parser:
                self.add_plugin('parser', vs2_parser)


    def add_plugin(self, plugin_type, plugin_class):
        """
        Add a plugin.
        """
        plg = plugin_class() if hasattr(plugin_class, 'SINGLETON') else plugin_class
        plg.api = self
        self.plugins[plugin_type].append(plg)
        self.plugins_by_name[plugin_class.__name__] = plg
        if issubclass(plugin_class, AnHookParser):
            if plugin_class.STRUCT not in self.hooks:
                self.hooks[plugin_class.STRUCT] = []
            self.hooks[plugin_class.STRUCT].append(plugin_class)
        if plugin_type == 'dataop':
            self.dataops[plg.name] = plg
            self.win.add_data_op(plg.name, is_fileop=plugin_class.FILEOP)
        self.dbg(f'Loaded parser {plugin_class.__name__}')


    def load(self, parent, filename, data=None, relation=None, virtual=False):
        """
        Load a file.

        :param parent: AnObject instance (None for newly opened files).
        """
        if (not virtual) and os.path.exists(filename) and not os.path.getsize(filename):
            self.warn(f'File {filename} is empty!')
            return
        data = self.objmgr.dataview(source=data or filename, filename=filename, parent=parent,
                relation=relation, virtual=virtual)
        self.win.open(data)
        self.dbg(f'Attempting to load {filename}...')
        self.loading_file, prev_loading = True, self.loading_file
        data.ext_parsers = []
        for parser in self.parsers:
            if issubclass(parser, AnHookParser):
                continue
            try:
                if issubclass(parser, AnInfo) or parser.can_parse(data.raw):
                    self.log(f'Processing {filename} with plugin {parser.__name__}...')
                    obj = parser(name=os.path.basename(filename), filename=filename, data=data, api=self)
                    for hook in self.hooks.get(parser.__name__, []):
                        if hook.present(obj):
                            hook(name=hook.__name__, api=self, data=data, parent=obj)
                    if not issubclass(parser, AnInfo):
                        obj.ext_parsers = data.ext_parsers
                        self.add_object(obj)
                    if hasattr(obj, 'extended_parse'):
                        data.ext_parsers.append(obj.extended_parse)
            except Exception as e:
                self.err(traceback.format_exc().strip().replace('\n', '<br>'))
                self.err(f'Failed parsing ({parser.__name__}): {e}!')
                continue
        self.loading_file = prev_loading
        for sec in self.tainted_info:
            self.win.update_info(sec)
        self.tainted_info = set()
        return data


    def load_embedded(self, parent, data, filename=None, ext=None, relation='embedded', virtual=True):
        ext = ('.' + ext) if ext else ''
        # line = make_internal_link(ext + " @ " + hex(offset), data.buffer_id or '*', offset, count=size) + f' ({size} bytes)'
        filename = filename or f'{parent.filename}.emb#{len(parent.children) + 1:03}{ext}'
        return self.load(parent=parent, filename=filename, data=data, relation=relation, virtual=virtual)


    def add_object(self, name_or_object, *args, recursive=True, **kwargs):
        """
        Add an object.
        """
        obj = self.objmgr.add_object(name_or_object, *args, **kwargs)
        winobj = self.win.add_tree_node(obj)
        winobj.an_id = obj.id
        if recursive:
            for child in obj.children:
                self.add_object(child, parent=obj, recursive=True)


    def get_object(self, id):
        """
        Retrieve an object by ID.
        """
        return self.objmgr.get_object(id)


    def get_buffer(self, buffer_id):
        """
        Get buffer (DataView) from .buffer_id.
        """
        return self.dataview(buffer_id)


    def dataview(self, source):
        """
        Get DataView based on buffer_id or filename, or create a new one from the given `bytes` buffer.
        """
        return self.objmgr.dataview(source)


    def add_info(self, info, section):
        """
        Add information.

        :param info: a structure containing strings, lists and dicts.
        """
        if isinstance(section, AnObject):
            section = section.data.buffer_id
        if isinstance(section, int):
            section = f'File #{section}'
        if section not in self.info:
            self.info[section] = []
        self.info[section].append(info)
        if not self.loading_file:
            self.win.update_info(section)
        else:
            self.tainted_info.add(section)


    def call_extended_parse(self, dataview):
        """
        Call .extended_parse() methods for a dataview.
        """
        for fun in dataview.ext_parsers:
            fun()
        for sec in self.tainted_info:
            self.win.update_info(sec)
        self.tainted_info = set()

