"""
AnTo plugin API: AnTo object base classes for plugins.

Author: Vlad Topan (vtopan/gmail)
"""
from ..api import lib
from ..core import AnObject

import ast
import inspect
import re
import struct


class AnParser(AnObject):
    """
    Base parser class.
    """
    FILTER = None   # overwrite this with a regex which will be used to test a buffer if it can be parsed (or implement `.can_parse()`)
    INFO = False

    @classmethod
    def can_parse(cls, data):
        """
        Returns True if this parser can parse the given DataView (bytes-like object).

        Override this in subclasses which could parse files.

        :param data: A DataView instance.
        """
        if cls.FILTER:
            if type(cls.FILTER) is str:
                cls.FILTER = cls.FILTER.encode('ascii')
            if type(cls.FILTER) is bytes:
                cls.FILTER = re.compile(cls.FILTER)
            return cls.FILTER.search(data)
        return False


    def parse(self):
        """
        The actual parsing (the bytes-like data is in `self.data` at offset `self.offset`).
        """
        raise NotImplementedError(f'Plugin class {self.__class__.__name__} must implement {inspect.stack()[0][3]}')



class AnCStructParser(AnParser):
    """
    Base class for C-struct parsers.
    """
    FIELD_NAMES = None              # override this with a list of field names
    STRUCT_FORMAT = None            # override this with a struct-like format (e.g. "<LLHBi")
    FIELD_OFFSETS = None            # (optional) override this with a list of field offsets


    def parse(self):
        """
        struct-based parser - assumes at least class.STRUCT_FORMAT is defined.
        """
        cls = self.__class__
        self.size = struct.calcsize(cls.STRUCT_FORMAT)
        data = self.data[self.offset:self.offset + self.size]
        if not cls.FIELD_OFFSETS:
            cls.FIELD_OFFSETS = lib.struct_field_offsets(cls.STRUCT_FORMAT)
        offsets = cls.FIELD_OFFSETS
        fields = struct.unpack(cls.STRUCT_FORMAT, data)
        for i, (name, value, offset) in enumerate(zip(cls.FIELD_NAMES, fields, offsets)):
            size = (offsets[i + 1] - offset) if i + 1 < len(offsets) else self.size - offset
            self.add_field(name, value=value, offset=self.offset + offset, size=size)
            setattr(self, name, value)



class AnHookParser(AnParser):
    """
    Base class for C-struct parsers.
    """
    STRUCT = None                   # override this with a structure name


    @classmethod
    def can_parse(cls, obj):
        """
        Check if this structure is present (apply base .can_parse() prefiltering first).
        """
        if not super().can_parse(obj):
            return False
        return self.present(cls, obj)


    @classmethod
    def present(cls, obj):
        """
        Return True if the item is present.

        :param obj: An instance of the cls.STRUCT type.
        """
        raise NotImplementedError(f'Plugin class {cls.__name__} must implement {inspect.stack()[0][3]}')



class AnInfo(AnParser):
    INFO = True



class AnDataOp:
    """
    Data parsing plugin.
    """
    CHOICES = None
    PARAMS = None
    INTYPE = bytes
    HTML = False
    OP = None
    SINGLETON = True
    FILEOP = False

    def __init__(self, choices=None, intype=None, **kwarg):
        self.params = kwarg or self.__class__.PARAMS
        self.choices = choices or self.__class__.CHOICES or {}
        self.intype = intype or self.__class__.INTYPE
        self.name = self.__class__.__name__
        if self.name.startswith('An') and (self.name[2].isupper() or self.name[2] == '_'):
            self.name = self.name[2:]
        self.name = self.name.strip('_').replace('_', ' ')
        if self.__class__.OP:
            sig = inspect.signature(self.__class__.OP)
            if 'source' in sig.parameters:
                self.op = self.__class__.OP
            else:
                self.op = lambda input, source: self.__class__.OP(input)


    def set_param(self, name, value):
        """
        Set an operation parameter.
        """
        if name in self.choices and value not in self.choices[name]:
            raise ValueError(f'Invalid value for parameter {name}: {value}!')
        self.params[name] = value


    def get_param(self, name):
        """
        Returns the value of a parameter.
        """
        return self.params[name]


    def set_choices(self, name, choices):
        """
        Set choices (possible values) for a parameter.
        """
        self.choices[name] = choices


    def get_choices(self, name):
        """
        Returns the list of choices for the given parameter, None if not set.
        """
        return self.choices.get(name)


    def op(self, input, source):
        """
        Run the operation on `input`, return `output`.

        :param input: The (pre-processed) buffer to work on.
        :param source: The source DataView for the data (if known).
        """
        raise NotImplementedError('Must be implemented in subclasses.')


    def run(self, input, source=None):
        """
        Internal function used to preprocess the input before running.
        """
        source = None
        if input is None:
            source = self.api.objmgr.gui_dataview()
            if source is None:
                raise ValueError('No buffer / file is selected!')
            input = source.raw
        if self.intype:
            if self.intype is str:
                input = input.decode('utf8')
            elif input is not None:
                input = self.intype(input)
        result = self.op(input, source)
        return result
