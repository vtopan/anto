"""
Miscellaneous functions, types and variables used by several libraries.

Author: Vlad Topan (vtopan/gmail)
"""
import os
import re
import struct


RX = {
    'utf16le': (rb'^([^\0]\0)+\0\0$',),
    'ascii': (rb'^[\r\n\t\x20-\x7E]+$',),
    'binary': (rb'[^\t\r\n\x20-\x7E]+',),
    'whitespace': (r'\s+',),
    'md_tt': (r'(^|\s)`([^\`]+)`',),
    'md_i': (r'(^|\s)\*([^\`]+)\*',),
    'md_b': (r'(^|\s)\*[^\*]([^\`]+)\*\*',),
    'md_u': (r'(^|\s)_[^\*]([^\_]+)_',),
    }
for k in RX:
    RX[k] = re.compile(*RX[k])


MIME_BINARY = 'application/octet-stream'


class ParseError(ValueError):
    pass



class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__dict__ = self



def dump_dict(title, d, indent=0):
    """
    Pretty-print dictionary whose values are other dicts, lists or basic types.
    """
    lines = []
    if title:
        lines.append(f'{indent * "  "}- {title}:')
    for k, v in d.items():
        if type(v) is dict:
            lines.append(dump_dict(k, v, indent + 1))
        elif type(v) is list:
            lines.append(f'{indent * "  "}  - {k}:')
            for i, e in enumerate(v):
                lines.append(dump_dict(f'#{i}', e, indent + 2))
        else:
            lines.append(f'{indent * "  "}  - {k}: {v}')
    return '\n'.join(lines)


def struct_field_offsets(format):
    """
    Calculate the offset of each field in a Python `struct`.
    """
    offs, result = 0, []
    for m in re.findall(r'\d*[a-z]', format.lstrip('<>='), flags=re.I):
        result.append(offs)
        offs += struct.calcsize(m)
    return result


def is_binary(data):
    """
    Check if any non-ASCII characters show up in the given bytes() data.
    """
    if isinstance(data, str):
        return False
    return bool(RX['binary'].search(data))


def to_hex(data, wrap=None, truncate=None, sep=' '):
    """
    Encode `bytes` (or `str`) string as hex.

    :param wrap: Wrap at this many characters (rounded to 3).
    :param truncate: If longer than this many characters, truncate and append an ellipsis.
    """
    if isinstance(data, str):
        data = data.encode('ascii')
    data = data.hex()
    data = sep.join(a + b for a, b in zip(data[::2], data[1::2]))
    if truncate and len(data) > truncate:
        return data[:truncate] + '[...]'
    mul = 2 + len(sep)
    if wrap:
        wrap = wrap // mul * mul
        data = '\n'.join(data[i:i + wrap].rstrip(' ') for i in range(0, len(data), wrap))
    return data


def from_hex(data):
    """
    Import `bytes` string from `str` containing hex-encoded data.
    """
    if isinstance(data, bytes):
        data = data.decode('ascii')
    return bytes.fromhex(RX['whitespace'].sub('', data))


def data_to_html(data, in_container=False, heading=1):
    """
    Render Python data (lists, strings, dicts etc.) as HTML.
    """
    if isinstance(data, str):
        if not in_container:
            data = f'<p>{data}</p>\n'
        # todo: interpret (more) basic markdown
        for k in ('tt', 'i', 'b', 'u'):
            data = RX[f'md_{k}'].sub(rf'\1<{k}>\2</{k}>', data)
        return data
    if isinstance(data, list):
        res = ['<ul>']
        for e in data:
            res.append(f'<li>{data_to_html(e, True, heading=heading)}</li>')
        return '\n'.join(res) + '\n</ul>\n'
    if isinstance(data, dict):
        res = []
        for k, v in sorted(data.items()):
            res.append(f'<h{heading}>{k.capitalize()}</h{heading}>\n{data_to_html(v, heading=heading + 1)}')
        return '\n'.join(res) + '\n'
    raise ValueError(f'Unknown data type {type(data)}!')


def find_files(path, ext=None, topdown=True, filter=None):
    """
    Find files in folder (recursively).

    :param ext: Extension (or list of extensions) without the period.
    :param filter: Callback which can return True for filenames to ignore.
    """
    if ext:
        if type(ext) is str:
            ext = [ext]
        ext = set(x.lower() for x in ext)
    for root, dirs, files in os.walk(path, topdown=topdown):
        for bn in files:
            fn = os.path.join(root, bn)
            if ext and fn.rsplit('.', 1)[-1].lower() not in ext:
                # computing this on full name rather than base name to avoid blacklisting filenames matching extensions
                continue
            if filter and not filter(fn):
                continue
            yield fn
