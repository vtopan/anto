"""
cparser: Tools for processing and importing C structure definitions.

Author: Vlad Topan (vtopan/gmail)
"""
import re


C_TYPES = {
    'B': ('byte', 'uchar',),
    'c': ('char', 'unsigned char',),
    'h': ('short',),
    'H': ('ushort', 'unsigned short', 'word'),
    'i': ('int', 'long',),
    'I': ('dword', 'uint', 'unsigned int', 'ulong',),
    'q': ('long long', ),
    'Q': ('ulonglong', 'unsigned long long',),
    }
C_TYPE_MAP = {e:k for k, v in C_TYPES.items() for e in v}

RX = {
    'struct':(r'typedef struct \w*\s*\{((?:\s*(?:[^;\{\}]+|union\s*\{[^\}]+?\}\s*\w*);\s*(?://[^\r\n]*\s*)?)*)\}([^;]+);', re.S),
    'enum':(r'enum\s*\w*\s*\{([\w\s,=]+?)\}\s*(\w+)\s*;',),
    'rawdefine':(r'^#define\s+(\w+)\s+(.+)', re.M),
    'param':(r'^\s*(_\w+)?\s+(\w+)\s*(?:(\*?)(\w+)\s*(?:\[([^\]]+)\])?[;,]?(\s*//[^\r\n]*\s*)?|\{([^\}]+)\}\s*(\w*);\s*)$', re.M|re.S),
    }
for k in RX:
    RX[k] = re.compile(*RX[k])


def parse_c_source(source):
    """
    Parses a C source and extracts constants and struct / union / enum typedefs.
    """
    constants, structs, unions = {}, {}, {}
    for name, val in RX['rawdefine'].findall(source):
        if '//' in val:
            val, comment = [x.strip() for x in val.split('//')]
        else:
            comment = ''
        if val.strip().startswith('{'):
            # assume it's an array => make a tuple
            val = val.replace('{', '(').replace('}', ')')
        constants[name] = (val, comment)
    for sdef, snames in RX['struct'].findall(source):
        sname = [x.strip() for x in snames.split(',')][0]
        # print('[*] Struct [%s]:' % sname)
        if sname in ('OVERLAPPED',):
            # ignored to avoid complex parsing (struct in union in struct)
            continue
        structs[sname] = crtstruct = []
        for m in RX['param'].findall(sdef):
            sal, ptype, ptr, pname, cnt, comment, ubody, uname = m
            # print('[-]   [%s] (%s%s)%s' % (pname or uname, ptype, ptr, comment or ''))
            if ubody:
                # union
                pname = uname or 'anon_union'
                uname = ptype = sname + '.' + pname     # todo: attempt to calculate the size for union fields in structs
                unions[uname] = crtunion = []
                for usal, uptype, uptr, upname, ucnt, ucomment, uubody, uuname in RX['param'].findall(ubody):
                    crtunion.append((upname, uptype, usal, ucomment.strip()[2:].strip()))
            crtstruct.append((pname, ptype, cnt, sal, comment.strip()[2:].strip()))
    return constants, structs, unions

