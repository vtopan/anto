"""
anto - Network functions.

Author: Vlad Topan (vtopan/gmail)
"""
import struct


def recv_int(s):
    """
    Recieve an int from a socket.
    """
    return struct.unpack('<i', s.recv(4))[0]


def send_int(s, value):
    """
    Recieve an int from a socket.
    """
    return s.send(struct.pack('<i', value))


def send_packed_data(s, *args):
    """
    Pack data (prefix with size) for sending over socket.
    """
    for data in args:
        if isinstance(data, int):
            send_int(s, data)
            continue
        if isinstance(data, str):
            data = data.encode()
        s.sendall(struct.pack('<i', len(data)) + data)


def recv_packed_data(s, count=1):
    """
    Receive packed data from socket.
    """
    res = []
    for i in range(count):
        size = struct.unpack('<i', s.recv(4))[0]
        res.append(recvall(s, size))
    return res[0] if count == 1 else res


def recvall(s, size):
    """
    Recieve a number of bytes.
    """
    received, chunks = 0, []
    while received < size:
        chunks.append(s.recv(size - received))
        received += len(chunks[-1])
    return b''.join(chunks)

