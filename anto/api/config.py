"""
AnTo configuration loader.

Author: Vlad Topan (vtopan/gmail)
"""
from .anlog import err
from .lib import AttrDict

import ast
import copy
import os
import pprint
import re


CONFIG_FILE = '$proj/anto.cfg'
CFG = AttrDict(path=AttrDict(), ui=AttrDict())
CFG.path.proj = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


def eval_path(path):
    """
    Interpolate a path using the values in CFG.path.
    """
    path = re.sub('\$(\w+)', lambda m: CFG.path.get(m.groups()[0], m.groups()[0]), path)
    return os.path.normpath(path)


def init_config(load_if_present=True):
    """
    Initialize the config object.

    :param load_if_present: Load the configuration from $PROJECT/anto.cfg if it exists.
    """
    CFG.path.cfgfile = eval_path(CONFIG_FILE)
    CFG.path.ext_bin = eval_path('$proj/anto/external/bin')
    if load_if_present and os.path.isfile(CFG.path.cfgfile):
        load_config()


def load_config():
    """
    Load the configuration file (CFG.path.cfgfile).
    """
    cfgfile = CFG.path.cfgfile
    if not os.path.isfile(cfgfile):
        raise OSError(f'Configuration file {cfgfile} not found!')
    try:
        data = open(cfgfile).read()
        if not data.strip():
            # empty file
            return
        data = ast.literal_eval(data)
    except Exception as e:
        err(f'Failed parsing configuration file: {e}!')
        raise
    for section, v in data.items():
        if section not in CFG:
            CFG[section] = AttrDict()
        for field, value in v.items():
            if section in ('path',):
                if field in ('cfgfile', 'proj'):
                    continue
                else:
                    CFG.path[field] = eval_path(value) if isinstance(value, str) else [eval_path(e) for e in value]
                    ro_section = 'ro_' + section
                    if ro_section not in CFG:
                        CFG[ro_section] = AttrDict()
                    CFG[ro_section][field] = value
            else:
                CFG[section][field] = value


def save_config():
    """
    Save the configuration.
    """
    cfg = copy.deepcopy(CFG)
    cfg['path'] = cfg.pop('ro_path', {})
    for e in ('cfgfile', 'proj'):
        if e in cfg['path']:
            del cfg['path'][e]
    open(CFG.path.cfgfile, 'w').write(pprint.pformat(cfg))
