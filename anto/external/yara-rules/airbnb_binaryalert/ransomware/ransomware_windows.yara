import "pe"

rule ransomware_windows_cerber_evasion
{
    meta:
        description = "Cerber Ransomware: Evades detection by machine learning applications"
        reference_1 = "http://blog.trendmicro.com/trendlabs-security-intelligence/cerber-starts-evading-machine-learning/"
        reference_2 = "http://www.darkreading.com/vulnerabilities---threats/cerber-ransomware-now-evades-machine-learning/d/d-id/1328506"
        author = "@fusionrace"
        md5 = "bc62b557d48f3501c383f25d014f22df"
    strings:
        $s1 = "38oDr5.vbs" fullword ascii wide
        $s2 = "8ivq.dll" fullword ascii wide
        $s3 = "jmsctls_progress32" fullword ascii wide
    condition:
        all of them
}
rule ransomware_windows_cryptolocker
{
    meta:
        description = "The CryptoLocker malware propagated via infected email attachments, and via an existing botnet; when activated, the malware encrypts files stored on local and mounted network drives"
        reference = "https://www.secureworks.com/research/cryptolocker-ransomware"
        author = "@fusionrace"
        md5 = "012d9088558072bc3103ab5da39ddd54"
    strings:
        $u0 = "Paysafecard is an electronic payment method for predominantly online shopping" fullword ascii wide
        $u1 = "bb to select the method of payment and the currency." fullword ascii wide
        $u2 = "Where can I purchase a MoneyPak?" fullword ascii wide
        $u3 = "Ukash is electronic cash and e-commerce brand." fullword ascii wide
        $u4 = "You have to send below specified amount to Bitcoin address" fullword ascii wide
        $u5 = "cashU is a prepaid online" fullword ascii wide
        $u6 = "Your important files \\b encryption" fullword ascii wide
        $u7 = "Encryption was produced using a \\b unique\\b0  public key" fullword ascii wide
        $u8 = "then be used to pay online, or loaded on to a prepaid card or eWallet." fullword ascii wide
        $u9 = "Arabic online gamers and e-commerce buyers." fullword ascii wide
    condition:
        2 of them
}
rule ransomware_windows_HDDCryptorA
{
    meta:
        description = "The HDDCryptor ransomware encrypts local harddisks as well as resources in network shares via Server Message Block (SMB)"
        reference = "http://blog.trendmicro.com/trendlabs-security-intelligence/bksod-by-ransomware-hddcryptor-uses-commercial-tools-to-encrypt-network-shares-and-lock-hdds/"
        author = "@fusionrace"
        md5 = "498bdcfb93d13fecaf92e96f77063abf"
    strings:
        // unique strings
        $u1 = "You are Hacked" fullword ascii wide
        $u2 = "Your H.D.D Encrypted , Contact Us For Decryption Key" nocase ascii wide
        $u3 = "start hard drive encryption..." ascii wide
        $u4 = "Your hard drive is securely encrypted" ascii wide
        // generic strings
        $g1 = "Wipe All Passwords?" ascii wide
        $g2 = "SYSTEM\\CurrentControlSet\\Services\\dcrypt\\config" ascii wide
        $g3 = "DiskCryptor" ascii wide
        $g4 = "dcinst.exe" fullword ascii wide
        $g5 = "dcrypt.exe" fullword ascii wide
        $g6 = "you can only use AES to encrypt the boot partition!" ascii wide
    condition:
        2 of ($u*) or 4 of ($g*)
}
rule ransomware_windows_hydracrypt
{
    meta:
        description = "HydraCrypt encrypts a victim’s files and appends the filenames with the extension “hydracrypt_ID_*"
        reference = "https://securingtomorrow.mcafee.com/mcafee-labs/hydracrypt-variant-of-ransomware-distributed-by-angler-exploit-kit/"
        author = "@fusionrace"
        md5 = "08b304d01220f9de63244b4666621bba"
    strings:
        $u0 = "oTraining" fullword ascii wide
        $u1 = "Stop Training" fullword ascii wide
        $u2 = "Play \"sound.wav\"" fullword ascii wide
        $u3 = "&Start Recording" fullword ascii wide
        $u4 = "7About record" fullword ascii wide
    condition:
        all of them
}

rule ransomware_windows_lazarus_wannacry
{
    meta:
        description = "Rule based on shared code between Feb 2017 Wannacry sample and Lazarus backdoor from Feb 2015 discovered by Neel Mehta"
        reference = "https://twitter.com/neelmehta/status/864164081116225536"
        author = "Costin G. Raiu, Kaspersky Lab"
        md5_1 = "9c7c7149387a1c79679a87dd1ba755bc"
        md5_2 = "ac21c8ad899727137c4b94458d7aa8d8"
    strings:
        $a1={
        51 53 55 8B 6C 24 10 56 57 6A 20 8B 45 00 8D 75
        04 24 01 0C 01 46 89 45 00 C6 46 FF 03 C6 06 01
        46 56 E8
        }

        $a2={
        03 00 04 00 05 00 06 00 08 00 09 00 0A 00 0D 00
        10 00 11 00 12 00 13 00 14 00 15 00 16 00 2F 00
        30 00 31 00 32 00 33 00 34 00 35 00 36 00 37 00
        38 00 39 00 3C 00 3D 00 3E 00 3F 00 40 00 41 00
        44 00 45 00 46 00 62 00 63 00 64 00 66 00 67 00
        68 00 69 00 6A 00 6B 00 84 00 87 00 88 00 96 00
        FF 00 01 C0 02 C0 03 C0 04 C0 05 C0 06 C0 07 C0
        08 C0 09 C0 0A C0 0B C0 0C C0 0D C0 0E C0 0F C0
        10 C0 11 C0 12 C0 13 C0 14 C0 23 C0 24 C0 27 C0
        2B C0 2C C0 FF FE
        }
    condition:
        ((uint16(0) == 0x5A4D)) and all of them
}
rule ransomware_windows_petya_variant_1
{
    meta:
        description = "Petya Ransomware new variant June 2017 using ETERNALBLUE"
        reference = "https://gist.github.com/vulnersCom/65fe44d27d29d7a5de4c176baba45759"
        author = "@fusionrace"
        md5 = "71b6a493388e7d0b40c83ce903bc6b04"
    strings:
        // instructions
        $s1 = "Ooops, your important files are encrypted." fullword ascii wide
        $s2 = "Send your Bitcoin wallet ID and personal installation key to e-mail" fullword ascii wide
        $s3 = "wowsmith123456@posteo.net. Your personal installation key:" fullword ascii wide
        $s4 = "Send $300 worth of Bitcoin to following address:" fullword ascii wide
        $s5 = "have been encrypted.  Perhaps you are busy looking for a way to recover your" fullword ascii wide
        $s6 = "need to do is submit the payment and purchase the decryption key." fullword ascii wide
    condition:
        any of them
}
rule ransomware_windows_petya_variant_2
{
    meta:
        description = "Petya Ransomware new variant June 2017 using ETERNALBLUE"
        reference = "https://gist.github.com/vulnersCom/65fe44d27d29d7a5de4c176baba45759"
        author = "@fusionrace"
        md5 = "71b6a493388e7d0b40c83ce903bc6b04"
    strings:
        // psexec disguised - applicable to s1
        $s1 = "dllhost.dat" fullword wide
        $s2 = "\\\\%ws\\admin$\\%ws" fullword wide
        $s3 = "%s /node:\"%ws\" /user:\"%ws\" /password:\"%ws\"" fullword wide
        $s4 = "\\\\.\\PhysicalDrive" fullword wide
        $s5 =  ".3ds.7z.accdb.ai.asp.aspx.avhd.back.bak.c.cfg.conf.cpp.cs.ctl.dbf.disk.djvu.doc.docx.dwg.eml.fdb.gz.h.hdd.kdbx.mail.mdb.msg.nrg.ora.ost.ova.ovf.pdf.php.pmf.ppt.pptx.pst.pvi.py.pyc.rar.rtf.sln.sql.tar.vbox.vbs.vcb.vdi.vfd.vmc.vmdk.vmsd.vmx.vsdx.vsv.work.xls.xlsx.xvd.zip." fullword wide
    condition:
        3 of them
}
rule ransomware_windows_petya_variant_3
{
    meta:
        description = "Petya Ransomware new variant June 2017 using ETERNALBLUE"
        reference = "https://gist.github.com/vulnersCom/65fe44d27d29d7a5de4c176baba45759"
        author = "@fusionrace"
        md5 = "71b6a493388e7d0b40c83ce903bc6b04"
    strings:
        $s1 = "wevtutil cl Setup & wevtutil cl System" fullword wide
        $s2 = "fsutil usn deletejournal /D %c:" fullword wide
    condition:
        any of them
}
rule ransomware_windows_petya_variant_bitcoin
{
    meta:
        description = "Petya Ransomware new variant June 2017 using ETERNALBLUE: Bitcoin"
        reference = "https://gist.github.com/vulnersCom/65fe44d27d29d7a5de4c176baba45759"
        author = "@fusionrace"
        md5 = "71b6a493388e7d0b40c83ce903bc6b04"
    strings:
        //Bitcoin address
        $s1 = "MIIBCgKCAQEAxP/VqKc0yLe9JhVqFMQGwUITO6WpXWnKSNQAYT0O65Cr8PjIQInTeHkXEjfO2n2JmURWV/uHB0ZrlQ/wcYJBwLhQ9EqJ3iDqmN19Oo7NtyEUmbYmopcq+YLIBZzQ2ZTK0A2DtX4GRKxEEFLCy7vP12EYOPXknVy/+mf0JFWixz29QiTf5oLu15wVLONCuEibGaNNpgq+CXsPwfITDbDDmdrRIiUEUw6o3pt5pNOskfOJbMan2TZu6zfhzuts7KafP5UA8/0Hmf5K3/F9Mf9SE68EZjK+cIiFlKeWndP0XfRCYXI9AJYCeaOu7CXF6U0AVNnNjvLeOn42LHFUK4o6JwIDAQAB" fullword wide
    condition:
        $s1
}
rule ransomware_windows_powerware_locky
{
    meta:
        description = "PowerWare Ransomware"
        reference = "https://researchcenter.paloaltonetworks.com/2016/07/unit42-powerware-ransomware-spoofing-locky-malware-family/"
        author = "@fusionrace"
        md5 = "3433a4da9d8794709630eb06afd2b8c1"
    strings:
        // unpacks the powershell script
        $s0 = "ScriptRunner.dll" fullword ascii wide
        // debug file
        $s1 = "ScriptRunner.pdb" fullword ascii wide
        // dropped powershell script
        $s2 = "fixed.ps1" fullword ascii wide
    condition:
        all of them
}
rule ransomware_windows_wannacry
{
    meta:
        description = "wannacry ransomware for windows"
        reference = "https://securelist.com/blog/incidents/78351/wannacry-ransomware-used-in-widespread-attacks-all-over-the-world/"
        author = "@fusionrace"
        md5 = "4fef5e34143e646dbf9907c4374276f5"
    strings:
        // generic
        $a1 = "msg/m_chinese" wide ascii
        $a2 = ".wnry" wide ascii
        $a3 = "attrib +h" wide ascii
        // unique malware strings
        $b1 = "WNcry@2ol7" wide ascii
        // c2
        $b2 = "iuqerfsodp9ifjaposdfjhgosurijfaewrwergwea.com" wide ascii
        // bitcoin addresses
        $b3 = "115p7UMMngoj1pMvkpHijcRdfJNXj6LrLn" wide ascii
        $b4 = "12t9YDPgwueZ9NyMgw519p7AA8isjr6SMw" wide ascii
        $b5 = "13AM4VW2dhxYgXeQepoHkHSQuy6NgaEb94" wide ascii
    condition:
        all of ($a*) or any of ($b*)
}
rule ransomware_windows_zcrypt
{
    meta:
        description = "Zcrypt will encrypt data and append the .zcrypt extension to the filenames"
        reference = "https://blog.malwarebytes.com/threat-analysis/2016/06/zcrypt-ransomware/"
        author = "@fusionrace"
        md5 = "d1e75b274211a78d9c5d38c8ff2e1778"
    strings:
        // unique strings
        $u1 = "How to Buy Bitcoins" ascii wide
        $u2 = "ALL YOUR PERSONAL FILES ARE ENCRYPTED" ascii wide
        $u3 = "Click Here to Show Bitcoin Address" ascii wide
        $u4 = "MyEncrypter2.pdb" fullword ascii wide
        // generic strings
        $g1 = ".p7b" fullword ascii wide
        $g2 = ".p7c" fullword ascii wide
        $g3 = ".pdd" fullword ascii wide
        $g4 = ".pef" fullword ascii wide
        $g5 = ".pem" fullword ascii wide
        $g6 = "How to decrypt files.html" fullword ascii wide
    condition:
        any of ($u*) or all of ($g*)
}
