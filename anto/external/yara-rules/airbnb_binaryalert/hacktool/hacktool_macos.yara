rule hacktool_macos_exploit_cve_5889
{
    meta:
        description = "http://www.cvedetails.com/cve/cve-2015-5889"
        reference = "https://www.exploit-db.com/exploits/38371/"
        author = "@mimeframe"
    strings:
        $a1 = "/etc/sudoers" fullword wide ascii
        $a2 = "/etc/crontab" fullword wide ascii
        $a3 = "* * * * * root echo" wide ascii
        $a4 = "ALL ALL=(ALL) NOPASSWD: ALL" wide ascii
        $a5 = "/usr/bin/rsh" fullword wide ascii
        $a6 = "localhost" fullword wide ascii
    condition:
        all of ($a*)
}
rule hacktool_macos_exploit_tpwn
{
    meta:
        description = "tpwn exploits a null pointer dereference in XNU to escalate privileges to root."
        reference = "https://www.rapid7.com/db/modules/exploit/osx/local/tpwn"
        author = "@mimeframe"
    strings:
        $a1 = "[-] Couldn't find a ROP gadget, aborting." wide ascii
        $a2 = "leaked kaslr slide," wide ascii
        $a3 = "didn't get root, but this system is vulnerable." wide ascii
        $a4 = "Escalating privileges! -qwertyoruiop" wide ascii
    condition:
        2 of ($a*)
}
rule hacktool_macos_juuso_keychaindump
{
    meta:
        description = "For reading OS X keychain passwords as root."
        reference = "https://github.com/juuso/keychaindump"
        author = "@mimeframe"
    strings:
        $a1 = "[-] Too many candidate keys to fit in memory" wide ascii
        $a2 = "[-] Could not allocate memory for key search" wide ascii
        $a3 = "[-] Too many credentials to fit in memory" wide ascii
        $a4 = "[-] The target file is not a keychain file" wide ascii
        $a5 = "[-] Could not find the securityd process" wide ascii
        $a6 = "[-] No root privileges, please run with sudo" wide ascii
    condition:
        4 of ($a*)
}
rule hacktool_macos_keylogger_b4rsby_swiftlog
{
    meta:
        description = "Dirty user level command line keylogger hacked together in Swift."
        reference = "https://github.com/b4rsby/SwiftLog"
        author = "@mimeframe"
    strings:
        $a1 = "You need to enable the keylogger in the System Prefrences" wide ascii
    condition:
        all of ($a*)
}
rule hacktool_macos_keylogger_caseyscarborough
{
    meta:
        description = "A simple and easy to use keylogger for macOS."
        reference = "https://github.com/caseyscarborough/keylogger"
        author = "@mimeframe"
    strings:
        $a1 = "/var/log/keystroke.log" wide ascii
        $a2 = "ERROR: Unable to create event tap." wide ascii
        $a3 = "Keylogging has begun." wide ascii
        $a4 = "ERROR: Unable to open log file. Ensure that you have the proper permissions." wide ascii
    condition:
        2 of ($a*)
}
rule hacktool_macos_keylogger_dannvix
{
    meta:
        description = "A simple keylogger for macOS."
        reference = "https://github.com/dannvix/keylogger-osx"
        author = "@mimeframe"
    strings:
        $a1 = "/var/log/keystroke.log" wide ascii
        $a2 = "<forward-delete>" wide ascii
        $a3 = "<unknown>" wide ascii
    condition:
        all of ($a*)
}
rule hacktool_macos_keylogger_eldeveloper_keystats
{
    meta:
        description = "A simple keylogger for macOS."
        reference = "https://github.com/ElDeveloper/keystats"
        author = "@mimeframe"
    strings:
        $a1 = "YVBKeyLoggerPerishedNotification" wide ascii
        $a2 = "YVBKeyLoggerPerishedByLackOfResponseNotification" wide ascii
        $a3 = "YVBKeyLoggerPerishedByUserChangeNotification" wide ascii
    condition:
        2 of ($a*)
}
rule hacktool_macos_keylogger_giacomolaw
{
    meta:
        description = "A simple keylogger for macOS."
        reference = "https://github.com/GiacomoLaw/Keylogger"
        author = "@mimeframe"
    strings:
        $a1 = "ERROR: Unable to access keystroke log file. Please make sure you have the correct permissions." wide ascii
        $a2 = "ERROR: Unable to create event tap." wide ascii
        $a3 = "Keystrokes are now being recorded" wide ascii
    condition:
        2 of ($a*)
}
rule hacktool_macos_keylogger_logkext
{
    meta:
        description = "LogKext is an open source keylogger for Mac OS X, a product of FSB software."
        reference = "https://github.com/SlEePlEs5/logKext"
        author = "@mimeframe"
    strings:
        // daemon
        $a1 = "logKextPassKey" wide ascii
        $a2 = "Couldn't get system keychain:" wide ascii
        $a3 = "Error finding secret in keychain" wide ascii
        $a4 = "com_fsb_iokit_logKext" wide ascii
        // client
        $b1 = "logKext Password:" wide ascii
        $b2 = "Logging controls whether the daemon is logging keystrokes (default is on)." wide ascii
        // logkextkeygen
        $c1 = "logKextPassKey" wide ascii
        $c2 = "Error: couldn't create secAccess" wide ascii
        // logkext
        $d1 = "IOHIKeyboard" wide ascii
        $d2 = "Clear keyboards called with kextkeys" wide ascii
        $d3 = "Added notification for keyboard" wide ascii
    condition:
        3 of ($a*) or all of ($b*) or all of ($c*) or all of ($d*)
}
rule hacktool_macos_keylogger_roxlu_ofxkeylogger
{
    meta:
        description = "ofxKeylogger keylogger."
        reference = "https://github.com/roxlu/ofxKeylogger"
        author = "@mimeframe"
    strings:
        $a1 = "keylogger_init" wide ascii
        $a2 = "install_keylogger_hook function not found in dll." wide ascii
        $a3 = "keylogger_set_callback" wide ascii
    condition:
        all of ($a*)
}
rule hacktool_macos_keylogger_skreweverything_swift
{
    meta:
        description = "It is a simple and easy to use keylogger for macOS written in Swift."
        reference = "https://github.com/SkrewEverything/Swift-Keylogger"
        author = "@mimeframe"
    strings:
        $a1 = "Can't create directories!" wide ascii
        $a2 = "Can't create manager" wide ascii
        $a3 = "Can't open HID!" wide ascii
        $a4 = "PRINTSCREEN" wide ascii
        $a5 = "LEFTARROW" wide ascii
    condition:
        4 of ($a*)
}

rule hacktool_macos_macpmem
{
    meta:
        description = "MacPmem enables read/write access to physical memory on macOS. Can be used by CSIRT teams and attackers."
        reference = "https://github.com/google/rekall/tree/master/tools/osx/MacPmem"
        author = "@mimeframe"
    strings:
        // osxpmem
        $a1 = "%s/MacPmem.kext" wide ascii
        $a2 = "The Pmem physical memory imager." wide ascii
        $a3 = "The OSXPmem memory imager." wide ascii
        $a4 = "These AFF4 Volumes will be loaded and their metadata will be parsed before the program runs." wide ascii
        $a5 = "Pmem driver version incompatible. Reported" wide ascii
        $a6 = "Memory access driver left loaded since you specified the -l flag." wide ascii
        // kext
        $b1 = "Unloading MacPmem" wide ascii
        $b2 = "MacPmem load tag is" wide ascii
    condition:
        MachO and 2 of ($a*) or all of ($b*)
}
rule hacktool_macos_manwhoami_icloudcontacts
{
    meta:
        description = "Pulls iCloud Contacts for an account. No dependencies. No user notification."
        reference = "https://github.com/manwhoami/iCloudContacts"
        author = "@mimeframe"
    strings:
        $a1 = "https://setup.icloud.com/setup/authenticate/" wide ascii
        $a2 = "https://p04-contacts.icloud.com/" wide ascii
        $a3 = "HTTP Error 401: Unauthorized. Are you sure the credentials are correct?" wide ascii
        $a4 = "HTTP Error 404: URL not found. Did you enter a username?" wide ascii
    condition:
        3 of ($a*)
}
rule hacktool_macos_manwhoami_mmetokendecrypt
{
    meta:
        description = "This program decrypts / extracts all authorization tokens on macOS / OS X / OSX."
        reference = "https://github.com/manwhoami/MMeTokenDecrypt"
        author = "@mimeframe"
    strings:
        $a1 = "security find-generic-password -ws 'iCloud'" wide ascii
        $a2 = "ERROR getting iCloud Decryption Key" wide ascii
        $a3 = "Could not find MMeTokenFile. You can specify the file manually." wide ascii
        $a4 = "Decrypting token plist ->" wide ascii
        $a5 = "Successfully decrypted token plist!" wide ascii
    condition:
        3 of ($a*)
}
rule hacktool_macos_manwhoami_osxchromedecrypt
{
    meta:
        description = "Decrypt Google Chrome / Chromium passwords and credit cards on macOS / OS X."
        reference = "https://github.com/manwhoami/OSXChromeDecrypt"
        author = "@mimeframe"
    strings:
        $a1 = "Credit Cards for Chrome Profile" wide ascii
        $a2 = "Passwords for Chrome Profile" wide ascii
        $a3 = "Unknown Card Issuer" wide ascii
        $a4 = "ERROR getting Chrome Safe Storage Key" wide ascii
        $b1 = "select name_on_card, card_number_encrypted, expiration_month, expiration_year from credit_cards" wide ascii
        $b2 = "select username_value, password_value, origin_url, submit_element from logins" wide ascii
    condition:
        3 of ($a*) or all of ($b*)
}
rule hacktool_macos_n0fate_chainbreaker
{
    meta:
        description = "chainbreaker can extract user credential in a Keychain file with Master Key or user password in forensically sound manner."
        reference = "https://github.com/n0fate/chainbreaker"
        author = "@mimeframe"
    strings:
        $a1 = "[!] Private Key Table is not available" wide ascii
        $a2 = "[!] Public Key Table is not available" wide ascii
        $a3 = "[-] Decrypted Private Key" wide ascii
    condition:
        all of ($a*)
}
rule hacktool_macos_ptoomey3_keychain_dumper
{
    meta:
        description = "Keychain dumping utility."
        reference = "https://github.com/ptoomey3/Keychain-Dumper"
        author = "@mimeframe"
    strings:
        $a1 = "keychain_dumper" wide ascii
        $a2 = "/var/Keychains/keychain-2.db" wide ascii
        $a3 = "<key>keychain-access-groups</key>" wide ascii
        $a4 = "SELECT DISTINCT agrp FROM genp UNION SELECT DISTINCT agrp FROM inet" wide ascii
        $a5 = "dumpEntitlements" wide ascii
    condition:
        all of ($a*)
}
