import "pe"
import "math"

private rule cobaltstrike_template_exe
{
    meta:
        description = "Template to provide executable detection Cobalt Strike payloads"
        reference = "https://www.cobaltstrike.com"
        author = "@javutin, @joseselvi"
    strings:
        $compiler = "mingw-w64 runtime failure" nocase

        $f1 = "VirtualQuery"   fullword
        $f2 = "VirtualProtect" fullword
        $f3 = "vfprintf"       fullword
        $f4 = "Sleep"          fullword
        $f5 = "GetTickCount"   fullword

        $c1 = { // Compare case insensitive with "msvcrt", char by char
                0f b6 50 01 80 fa 53 74 05 80 fa 73 75 42 0f b6
                50 02 80 fa 56 74 05 80 fa 76 75 34 0f b6 50 03
                80 fa 43 74 05 80 fa 63 75 26 0f b6 50 04 80 fa
                52 74 05 80 fa 72 75 18 0f b6 50 05 80 fa 54 74
        }
    condition:
        uint16(0) == 0x5a4d and
        filesize < 1000KB and
        $compiler and
        all of ($f*) and
        all of ($c*)
}

rule hacktool_windows_cobaltstrike_artifact_exe
{
    meta:
        description = "Detection of the Artifact payload from Cobalt Strike"
        reference = "https://www.cobaltstrike.com/help-artifact-kit"
        author = "@javutin, @joseselvi"
    condition:
        cobaltstrike_template_exe and
        filesize < 100KB and
        pe.sections[pe.section_index(".data")].raw_data_size > 512 and
        math.entropy(pe.sections[pe.section_index(".data")].raw_data_offset, 512 ) >= 7
}


private rule cobaltstrike_beacon_raw
{
    strings:
        $s1 = "%d is an x64 process (can't inject x86 content)" fullword
        $s2 = "Failed to impersonate logged on user %d (%u)" fullword
        $s3 = "powershell -nop -exec bypass -EncodedCommand \"%s\"" fullword
        $s4 = "IEX (New-Object Net.Webclient).DownloadString('http://127.0.0.1:%u/'); %s" fullword
        $s5 = "could not run command (w/ token) because of its length of %d bytes!" fullword
        $s6 = "could not write to process memory: %d" fullword
        $s7 = "%s.4%08x%08x%08x%08x%08x.%08x%08x%08x%08x%08x%08x%08x.%08x%08x%08x%08x%08x%08x%08x.%08x%08x%08x%08x%08x%08x%08x.%x%x.%s" fullword
        $s8 = "Could not connect to pipe (%s): %d" fullword

        $b1 = "beacon.dll"     fullword
        $b2 = "beacon.x86.dll" fullword
        $b3 = "beacon.x64.dll" fullword

    condition:
        uint16(0) == 0x5a4d and
        filesize < 1000KB and
        (
            any of ($b*) or
            5 of ($s*)
        )
}

private rule cobaltstrike_beacon_exe
{
    condition:
        cobaltstrike_template_exe and
        filesize > 100KB and filesize < 500KB and
        pe.sections[pe.section_index(".data")].raw_data_size > 200000 and
        math.entropy(pe.sections[pe.section_index(".data")].raw_data_offset + 1024, 150000 ) >= 7 
}

private rule cobaltstrike_beacon_b64
{
    strings:
        $s1a = "JWQgaXMgYW4geDY0IHByb2Nlc3MgKGNhbid0IGluam"
        $s1b = "ZCBpcyBhbiB4NjQgcHJvY2VzcyAoY2FuJ3QgaW5qZW"
        $s1c = "IGlzIGFuIHg2NCBwcm9jZXNzIChjYW4ndCBpbmplY3"

        $s2a = "RmFpbGVkIHRvIGltcGVyc29uYXRlIGxvZ2dlZCBvbi"
        $s2b = "YWlsZWQgdG8gaW1wZXJzb25hdGUgbG9nZ2VkIG9uIH"
        $s2c = "aWxlZCB0byBpbXBlcnNvbmF0ZSBsb2dnZWQgb24gdX"

        $s3a = "cG93ZXJzaGVsbCAtbm9wIC1leGVjIGJ5cGFzcyAtRW"
        $s3b = "b3dlcnNoZWxsIC1ub3AgLWV4ZWMgYnlwYXNzIC1Fbm"
        $s3c = "d2Vyc2hlbGwgLW5vcCAtZXhlYyBieXBhc3MgLUVuY2"

        $s4a = "SUVYIChOZXctT2JqZWN0IE5ldC5XZWJjbGllbnQpLk"
        $s4b = "RVggKE5ldy1PYmplY3QgTmV0LldlYmNsaWVudCkuRG"
        $s4c = "WCAoTmV3LU9iamVjdCBOZXQuV2ViY2xpZW50KS5Eb3"

    condition:
        filesize < 1000KB and
        5 of ($s*)
}

rule hacktool_windows_cobaltstrike_beacon
{
    meta:
        description = "Detection of the Beacon payload from Cobalt Strike"
        reference = "https://www.cobaltstrike.com/help-beacon"
        author = "@javutin, @joseselvi"
    condition:
        cobaltstrike_beacon_b64 or
        cobaltstrike_beacon_raw or
        cobaltstrike_beacon_exe
}

rule hacktool_windows_cobaltstrike_postexploitation
{
    meta:
        description = "Detection of strings in the post-exploitation modules of Cobalt Strike"
        reference = "https://www.cobaltstrike.com/support"
        author = "@javutin, @mimeframe"
    strings:
        $s1 = "\\devcenter\\aggressor\\external\\"

    condition:
        filesize > 10KB and filesize < 1000KB and
        all of ($s*)
}

rule hacktool_windows_cobaltstrike_powershell
{
    meta:
        description = "Detection of the PowerShell payloads from Cobalt Strike"
        reference = "https://www.cobaltstrike.com/help-payload-generator"
        author = "@javutin, @joseselvi"
    strings:
        $ps1 = "Set-StrictMode -Version 2"
        $ps2 = "func_get_proc_address"
        $ps3 = "func_get_delegate_type"
        $ps4 = "FromBase64String"
        $ps5 = "VirtualAlloc"
        $ps6 = "var_code"
        $ps7 = "var_buffer"
        $ps8 = "var_hthread"

    condition:
        $ps1 at 0 and
        filesize < 1000KB and
        all of ($ps*)
}

rule hacktool_windows_hot_potato
{
    meta:
        description = "https://foxglovesecurity.com/2016/01/16/hot-potato/"
        reference = "https://github.com/foxglovesec/Potato"
        author = "@mimeframe"
    strings:
        $a1 = "Parsing initial NTLM auth..." wide ascii
        $a2 = "Got PROPFIND for /test..." wide ascii
        $a3 = "Starting NBNS spoofer..." wide ascii
        $a4 = "Exhausting UDP source ports so DNS lookups will fail..." wide ascii
        $a5 = "Usage: potato.exe -ip" wide ascii
    condition:
        any of ($a*)
}

rule hacktool_windows_mimikatz_copywrite
{
    meta:
        description = "Mimikatz credential dump tool: Author copywrite"
        reference = "https://github.com/gentilkiwi/mimikatz"
        author = "@fusionrace"
        md5_1 = "0c87c0ca04f0ab626b5137409dded15ac66c058be6df09e22a636cc2bcb021b8"
        md5_2 = "0c91f4ca25aedf306d68edaea63b84efec0385321eacf25419a3050f2394ee3b"
        md5_3 = "0fee62bae204cf89d954d2cbf82a76b771744b981aef4c651caab43436b5a143"
        md5_4 = "004c07dcd04b4e81f73aacd99c7351337f894e4dac6c91dcfaadb4a1510a967c"
        md5_5 = "09c542ff784bf98b2c4899900d4e699c5b2e2619a4c5eff68f6add14c74444ca"
        md5_6 = "09054be3cc568f57321be32e769ae3ccaf21653e5d1e3db85b5af4421c200669"
    strings:
        $s1 = "Kiwi en C" fullword ascii wide
        $s2 = "Benjamin DELPY `gentilkiwi`" fullword ascii wide
        $s3 = "http://blog.gentilkiwi.com/mimikatz" fullword ascii wide
        $s4 = "Build with love for POC only" fullword ascii wide
        $s5 = "gentilkiwi (Benjamin DELPY)" fullword wide
        $s6 = "KiwiSSP" fullword wide
        $s7 = "Kiwi Security Support Provider" fullword wide
        $s8 = "kiwi flavor !" fullword wide
    condition:
        any of them
}

rule hacktool_windows_mimikatz_errors
{
    meta:
        description = "Mimikatz credential dump tool: Error messages"
        reference = "https://github.com/gentilkiwi/mimikatz"
        author = "@fusionrace"
        md5_1 = "09054be3cc568f57321be32e769ae3ccaf21653e5d1e3db85b5af4421c200669"
        md5_2 = "004c07dcd04b4e81f73aacd99c7351337f894e4dac6c91dcfaadb4a1510a967c"
    strings:
        $s1 = "[ERROR] [LSA] Symbols" fullword ascii wide
        $s2 = "[ERROR] [CRYPTO] Acquire keys" fullword ascii wide
        $s3 = "[ERROR] [CRYPTO] Symbols" fullword ascii wide
        $s4 = "[ERROR] [CRYPTO] Init" fullword ascii wide
    condition:
        all of them
}

rule hacktool_windows_mimikatz_files
{
    meta:
        description = "Mimikatz credential dump tool: Files"
        reference = "https://github.com/gentilkiwi/mimikatz"
        author = "@fusionrace"
        md5_1 = "09054be3cc568f57321be32e769ae3ccaf21653e5d1e3db85b5af4421c200669"
        md5_2 = "004c07dcd04b4e81f73aacd99c7351337f894e4dac6c91dcfaadb4a1510a967c"
    strings:
        $s1 = "kiwifilter.log" fullword wide
        $s2 = "kiwissp.log" fullword wide
        $s3 = "mimilib.dll" fullword ascii wide
    condition:
        any of them
}

rule hacktool_windows_mimikatz_modules
{
    meta:
        description = "Mimikatz credential dump tool: Modules"
        reference = "https://github.com/gentilkiwi/mimikatz"
        author = "@fusionrace"
        md5_1 = "0c87c0ca04f0ab626b5137409dded15ac66c058be6df09e22a636cc2bcb021b8"
        md5_2 = "0c91f4ca25aedf306d68edaea63b84efec0385321eacf25419a3050f2394ee3b"
        md5_3 = "09054be3cc568f57321be32e769ae3ccaf21653e5d1e3db85b5af4421c200669"
        md5_4 = "004c07dcd04b4e81f73aacd99c7351337f894e4dac6c91dcfaadb4a1510a967c"
        md5_5 = "0fee62bae204cf89d954d2cbf82a76b771744b981aef4c651caab43436b5a143"
    strings:
        $s1 = "mimilib" fullword ascii wide
        $s2 = "mimidrv" fullword ascii wide
        $s3 = "mimilove" fullword ascii wide
    condition:
        any of them
}

rule hacktool_windows_mimikatz_sekurlsa
{
    meta:
        description = "Mimikatz credential dump tool"
        reference = "https://github.com/gentilkiwi/mimikatz"
        author = "@fusionrace"
        SHA256_1 = "09054be3cc568f57321be32e769ae3ccaf21653e5d1e3db85b5af4421c200669"
        SHA256_2 = "004c07dcd04b4e81f73aacd99c7351337f894e4dac6c91dcfaadb4a1510a967c"
    strings:
        $s1 = "dpapisrv!g_MasterKeyCacheList" fullword ascii wide
        $s2 = "lsasrv!g_MasterKeyCacheList" fullword ascii wide
        $s3 = "!SspCredentialList" ascii wide
        $s4 = "livessp!LiveGlobalLogonSessionList" fullword ascii wide
        $s5 = "wdigest!l_LogSessList" fullword ascii wide
        $s6 = "tspkg!TSGlobalCredTable" fullword ascii wide
    condition:
        all of them
}

rule hacktool_windows_moyix_creddump
{
    meta:
        description = "creddump is a python tool to extract credentials and secrets from Windows registry hives."
        reference = "https://github.com/moyix/creddump"
        author = "@mimeframe"
    strings:
        $a1 = "!@#$%^&*()qwertyUIOPAzxcvbnmQQQQQQQQQQQQ)(*@&%" wide ascii
        $a2 = "0123456789012345678901234567890123456789" wide ascii
        $a3 = "NTPASSWORD" wide ascii
        $a4 = "LMPASSWORD" wide ascii
        $a5 = "aad3b435b51404eeaad3b435b51404ee" wide ascii
        $a6 = "31d6cfe0d16ae931b73c59d7e0c089c0" wide ascii
    condition:
        all of ($a*)
}

rule hacktool_windows_ncc_wmicmd
{
    meta:
        description = "Command shell wrapper for WMI"
        reference = "https://github.com/nccgroup/WMIcmd"
        author = "@mimeframe"
    strings:
        $a1 = "Need to specify a username, domain and password for non local connections" wide ascii
        $a2 = "WS-Management is running on the remote host" wide ascii
        $a3 = "firewall (if enabled) allows connections" wide ascii
        $a4 = "WARNING: Didn't see stdout output finished marker - output may be truncated" wide ascii
        $a5 = "Command sleep in milliseconds - increase if getting truncated output" wide ascii
        $b1 = "0x800706BA" wide ascii
        $b2 = "NTLMDOMAIN:" wide ascii
        $b3 = "cimv2" wide ascii
    condition:
        any of ($a*) or all of ($b*)
}

rule hacktool_windows_rdp_cmd_delivery
{
    meta:
        description = "Delivers a text payload via RDP (rubber ducky)"
        reference = "https://github.com/nopernik/mytools/blob/master/rdp-cmd-delivery.sh"
        author = "@fusionrace"
    strings:
        $s1 = "Usage: rdp-cmd-delivery.sh OPTIONS" ascii wide
        $s2 = "[--tofile 'c:\\test.txt' local.ps1 #will copy contents of local.ps1 to c:\\test.txt" ascii wide
        $s3 = "-cmdfile local.bat                #will execute everything from local.bat" ascii wide
        $s4 = "To deliver powershell payload, use '--cmdfile script.ps1' but inside powershell console" ascii wide
    condition:
        any of them
}

rule hacktool_windows_wmi_implant
{
    meta:
        description = "A PowerShell based tool that is designed to act like a RAT"
        reference = "https://www.fireeye.com/blog/threat-research/2017/03/wmimplant_a_wmi_ba.html"
        author = "@fusionrace"
    strings:
        $s1 = "This really isn't applicable unless you are using WMImplant interactively." fullword ascii wide
        $s2 = "What command do you want to run on the remote system? >" fullword ascii wide
        $s3 = "Do you want to [create] or [delete] a string registry value? >" fullword ascii wide
        $s4 = "Do you want to run a WMImplant against a list of computers from a file? [yes] or [no] >" fullword ascii wide
        $s5 = "What is the name of the service you are targeting? >" fullword ascii wide
        $s6 = "This function enables the user to upload or download files to/from the attacking machine to/from the targeted machine" fullword ascii wide
        $s7 = "gen_cli - Generate the CLI command to execute a command via WMImplant" fullword ascii wide
        $s8 = "exit - Exit WMImplant" fullword ascii wide
        $s9 = "Lateral Movement Facilitation" fullword ascii wide
        $s10 = "vacant_system - Determine if a user is away from the system." fullword ascii wide
        $s11 = "Please provide the ProcessID or ProcessName flag to specify the process to kill!" fullword ascii wide
    condition:
        any of them
}
