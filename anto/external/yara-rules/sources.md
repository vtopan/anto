# github / bartblaze / Yara-rules

- <https://github.com/bartblaze/Yara-rules> @commit eab2355
- license: MIT (`./bartblaze_Yara-rules/LICENSE`)

# github / airbnb / binaryalert

- <https://github.com/airbnb/binaryalert/tree/master/rules/public> @commit 733675d
- license: Apache 2.0 (`./airbnb_binaryalert/LICENSE`)

# github / DiabloHorn / yara4pentesters

- <https://github.com/DiabloHorn/yara4pentesters/blob/master/juicy_files.txt> @commit c53feac
- license: MIT (`./DiabloHorn_yara4pentesters/LICENSE`)

# github / tenable / yara-rules

- <https://github.com/tenable/yara-rules> @commit b4d20de
- license: BSD (`./tenable_yara-rules/LICENSE`)

# github / sophos-ai / yaraml_rules

- <https://github.com/sophos-ai/yaraml_rules/tree/master/generic_powershell_detector_jan28_2020> @commit 3a2e145
- license: Apache 2.0 (`./sophos-ai_yaraml_rules/LICENSE`)

# github / malice-plugins / yara

- <https://github.com/malice-plugins/yara/tree/master/rules> @commit ed703fb
- license: MIT + Apache 2.0 (`./malice-plugins_yara/LICENSE`)

# Other YARA rule sources (not included)

- <https://github.com/Yara-Rules/rules/>
- <https://github.com/fr0gger/Yara-Unprotect>
