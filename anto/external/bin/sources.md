# Yara

- binaries from Yara releases on github compressed with UPX
- current version: 3.11.0-994
- `yara64.exe` & `yarac64.exe`:
    <https://github.com/VirusTotal/yara/releases/download/v3.11.0/yara-v3.11.0-994-win64.zip>
- license in `LICENSE-YARA` (3-clause BSD)
