"""
AnObjMgr: AnTo object manager.

Author: Vlad Topan (vtopan/gmail)
"""
from .anobject import AnObject
from .dataview import DataView

import hashlib
import os


class AnObjMgr:
    CRT_FILEID = 0


    def __init__(self, api=None):
        self.obj_map = {}
        self.known_buffers = set()      # known `DataView.buffer_id`s
        self.gui_buffer_id = None
        self.api = api


    def add_object(self, name_or_object, data=None, parent=None, *args, **kwargs):
        """
        Add an object.
        """
        if parent:
            if not hasattr(parent, 'id'):
                parent = self.obj_map[parent]
        if type(data) is str:
            data = self.dataview(data)
        if type(name_or_object) is str:
            name = name_or_object
            obj = AnObject(name, *args, data=data, parent=parent, api=self.api, **kwargs)
        else:
            obj = name_or_object
            name = obj.name
            obj.parent = parent
        obj.mgr = self
        self.obj_map[obj.id] = obj
        return obj


    def set_gui_buffer(self, buffer_id):
        """
        Set the current buffer displayed in the GUI.
        """
        self.gui_buffer_id = buffer_id


    def gui_dataview(self):
        """
        Returns the currently selected DataView in the GUI (or None).
        """
        if self.gui_buffer_id is not None:
            return self.dataview(self.gui_buffer_id)


    # def current_filename(self):
    #     """
    #     Returns the filename of the last accessed file.
    #     """
    #     f = self.current_dataview()
    #     if not f.filename:
    #         return None
    #     return f.filename


    def get_object(self, id):
        """
        Retrieve an object by ID (or None if unknown).
        """
        return self.obj_map.get(id)


    def dataview(self, source, filename=None, select=False, **kwargs):
        """
        Returns a DataView object for the given filename or buffer_id, or creates a new one
        if given a buffer.

        :param source: `buffer_id` (int), filename (str) or buffer (bytes/bytearray).
        :param filename: if not given as `source`, place filename in this parameter.
        """
        if type(source) is int:
            return DataView.open(buffer_id=source)
        if not source:
            raise ValueError('A data buffer or filename is required!')
        if type(source) in (bytes, bytearray):
            dv = DataView.open(filename=filename, data=source, **kwargs)
        else:
            if not os.path.getsize(source):
                raise ValueError(f'Invalid or empty file: {source}!')
            dv = DataView.open(filename=source, **kwargs)
        if dv.buffer_id not in self.known_buffers:
            dv.hashes = {k:getattr(hashlib, k)() for k in ('md5', 'sha1', 'sha256')}
            chunk_size = 256 * 1024
            hashes = dv.hashes.values()
            for offs in range(0, len(dv), chunk_size):
                chunk = dv[offs:offs + chunk_size]
                for h in hashes:
                    h.update(chunk)
            for k, v in dv.hashes.items():
                dv.hashes[k] = v.hexdigest().upper()
        return dv
