"""
AnField: Object field abstraction.

Author: Vlad Topan (vtopan/gmail)
"""


class AnField:

    def __init__(self, name, value=None, offset=0, size=None, parent=None, actions=None, description=None, hint=None):
        self.name = name
        self.value = value
        self.offset = offset
        self.size = size
        self.parent = parent
        self.actions = actions
        self.description = description
        self.hint = hint
