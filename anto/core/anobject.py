"""
AnObject: AnTo object abstraction / base class.

Author: Vlad Topan (vtopan/gmail)
"""
from .anfield import AnField
from .dataview import DataView

import os
import threading


class AnObject:
    _NextObjId = 0
    NEXT_ID_LOCK = threading.Lock()

    def __init__(self, name=None, filename=None, data=None, parent=None, children=None, actions=None,
                fields=None, obj_type=None, offset=0, size=None, description=None, aliases=None,
                hint=None, api=None, depth=0):
        if not (name or filename):
            raise ValueError('Either `name` or `filename` must be passed to AnObject()!')
        self.filename = filename
        self.name = name or self.__class__.__name__
        self.aliases = [] + (aliases or [])
        if self.name.endswith(']'):
            self.aliases.append(self.name.rsplit('[', 1)[0])
        if not isinstance(data, DataView):
            data = DataView.open(data=data)
        self.data = data
        AnObject.NEXT_ID_LOCK.acquire()
        self.id = AnObject._NextObjId
        AnObject._NextObjId += 1
        AnObject.NEXT_ID_LOCK.release()
        self.parent = parent
        self.children = children if children is not None else []
        self.fields = {}
        self.children_by_name = {}
        self.actions = actions
        self.obj_type = obj_type or self.__class__.__name__
        self.offset, self.size = offset, size
        self.description = description
        self.hint = hint
        self.api = api
        self.depth = depth
        if parent:
            parent.add_child(self)
        if fields:
            for field, params in fields.items():
                self.add_field(name=field, **params)
        if hasattr(self, 'preprocess'):
            self.preprocess()
        if hasattr(self, 'parse'):
            self.parse()


    def add_child(self, obj, is_list=False):
        """
        Add a child object.
        """
        if not obj.api:
            obj.api = self.api
        obj.depth = self.depth + 1
        for name in [obj.name] + obj.aliases:
            if is_list or name in self.children_by_name:
                if (not is_list) and not isinstance(self.children_by_name[name], list):
                    self.children_by_name[name] = [self.children_by_name[name]]
                self.children_by_name[name].append(obj)
            else:
                self.children_by_name[name] = obj
        self.children.append(obj)


    def remove_child(self, obj):
        """
        Remove a child object.
        """
        self.children.remove(obj)


    def add_field(self, name, is_list=False, **kwargs):
        """
        Add a field.
        """
        field = AnField(name, **kwargs)
        if name in self.fields or is_list:
            if (not is_list) and not isinstance(self.fields[name], list):
                self.fields[name] = [self.fields[name]]
            self.fields[name].append(field)
        else:
            self.fields[name] = field
        return field


    def add_info(self, info):
        """
        Add an info string to this object's DataView.
        """
        self.api.add_info(info, self)


    def __str__(self):
        return f'{self.__class__.__name__}<{self.name}>'


    def __getitem__(self, key):
        if key in self.fields:
            return self.fields[key]
        if key in self.children_by_name:
            return self.children_by_name[key]
        raise IndexError(f'{key} is not a field or direct child of {self.name}!')


    def __getattr__(self, key):
        if key[0] != '_':
            try:
                return self[key]
            except IndexError:
                pass
        return super().__getattribute__(key)
