#!/usr/bin/env python3
"""
mainwindow: AnTo main window.

Author: Vlad Topan (vtopan/gmail)
"""
from .libqthexed import QHexEditor
from libioqt.libioqt import create_layouts, VLabelEdit, VTable
from iolib import AttrDict
from iolib.net import PCap, PROTOCOLS
from ..api import save_config, to_hex, from_hex, CFG, runner
from anto.plugins.dataop.pcap import extract_streams_from_pcap
from anto.plugins.dataop.file import run_file

from PySide6 import QtGui, QtWidgets, QtCore
from PySide6.QtWidgets import QTableWidgetItem, QHBoxLayout, QVBoxLayout, QLabel, QTreeWidget, QTableWidget, \
        QAbstractScrollArea, QMenuBar, QWidget, QTabWidget, QSplitter, QCheckBox, QTextBrowser, QStatusBar, \
        QPushButton, QLineEdit, QHeaderView
from PySide6.QtCore import Qt, SIGNAL, Slot, Signal, QModelIndex, QItemSelectionModel, QMimeData, \
        QRegularExpression
from PySide6.QtGui import QFontDatabase, QFont, QAction

import functools
import os
import re
import sys
import time
import traceback


PROJ_PATH = os.path.abspath(f'{os.path.dirname(__file__)}/../..').replace('\\', '/')
RECENT_FILES = '&Recent files'
MENU = {
    '&File': {
        '&Open': 'F3',
        '&Close': 'Ctrl+F3',
        '-1': None,
        RECENT_FILES: {},
        '-1': None,
        '&Quit': 'Ctrl+Q',
    },
    '&Help': {
        '&About': 'F1',
    }
}

INFO_CSS = '''
tt {color: #333;}
h1 {font: 14pt;}
h2 {font-size: 12pt; color: #226;}
h3 {font-size: 11pt;}
h4 {font-size: 10pt;}
'''

DARK_CSS = '''
* {
  background-color: #000;
  border-color: #AA8;
  color: #CC8;
}

QCheckBox {
  border-color: #AA8;
}

QCheckBox::indicator {
  border-color: #AA8;
  border: 1px solid;
}

QCheckBox::indicator:checked {
  color: #000;
  image: url({PROJ_PATH}/anto/data/img/checked.png);
}

QCheckBox::indicator:checked:hover {
  background-color: #600;
}

QCheckBox::indicator:indeterminate:hover {
  background-color: #600;
  background-color: #600;
}

QCheckBox::indicator:pressed {
  background-color: #600;
}

QCheckBox::indicator:unchecked {
  background-color: #000;
}

QCheckBox::indicator:unchecked:hover {
  background-color: #600;
}

QCheckBox:indicator {
  background-color: #320;
}

QComboBox {
  background-color: #210;
}

QHeaderView::section {
  background-color: #000;
}

QLineEdit {
  background-color: #210;
  border-color: #AA8;
  border: 1px solid;
  padding: 0px;
}

QLineEdit:hover {
  background-color: #600;
}

QListView::item:hover {
  background-color: #600;
}

QListView::item:selected {
  background-color: #630;
}

QListWidget {
  background-color: #210;
  border-color: #AA8;
}

QPushButton {
  background-color: #320;
}

QPushButton:hover {
  background-color: #600;
}

QTabBar::tab {
  background-color: #000;
  border-color: #AA8;
  border-radius: 4px;
  border-style: solid;
  border-width: 1px;
  padding: 2px 5px 2px 5px;
}

QTabBar::tab:selected {
  background-color: #630;
}

QTabBar:hover {
  background-color: #600;
}

QTabWidget {
  border-color: #AA8;
}

QTableWidget {
  outline: 0;
}

QTableWidget::item {
  border: 1px solid;
}

QTableWidget::item:hover {
  background-color: #600;
}

QTableWidget::item:selected {
  background-color: #630;
}

QTableWidget::selected {
  border: 1px solid;
}

QToolTip {
  background-color: #600;
  border: 0;
}
'''.replace('{PROJ_PATH}', PROJ_PATH)


def menu_from_dict(win, menu, mdict):
    """
    Creates a menu based on a dict template.

    :param win: Window (Qt object with the `menu_...()` action handlers).
    """
    if not hasattr(win, 'menus'):
        win.menus = {}
    for k, v in mdict.items():
        if k.startswith('-'):
            menu.addSeparator()
        else:
            if type(v) is dict:
                submenu = QtWidgets.QMenu(k)
                win.menus[k] = submenu
                menu.addMenu(submenu)
                menu_from_dict(win, submenu, v)
            else:
                handler = getattr(win, 'menu_' + re.sub(r'\W+', '', k), None)
                if handler:
                    action = menu.addAction(QAction(k, win, triggered=handler, shortcut=v))
                    win.menus[k] = action



class AntoMain(QtWidgets.QMainWindow):
    """
    Main window.

    :param app: QApplication.
    """

    set_ui_text_sig = Signal(str, str)


    def __init__(self, app):
        super().__init__()
        self.app = app
        self.main = QWidget()  # central widget
        self.resfolder = f'{os.path.dirname(__file__)}/../res'
        font_id = QFontDatabase.addApplicationFont(f'{self.resfolder}/dosfont.ttf')
        self.dosfont = QFont(QFontDatabase.applicationFontFamilies(font_id)[0], 12)
        self.fonts = {
            'fixed': self.dosfont,  # QtGui.QFont('Terminal', 8)
            }
        self.open_files = {}
        self.pcap = None
        self.setWindowIcon(QtGui.QIcon(f'{self.resfolder}/anto64.ico'))
        self.setCentralWidget(self.main)
        ## window elements
        self.hexview = QHexEditor(b' ', font=self.fonts['fixed'])
        self.lbl_tree_objects = QLabel('Objects:')
        self.tree_objects = QTreeWidget()
        self.tree_objects.setColumnCount(3)
        self.tree_objects.setHeaderLabels(['Name', 'Offset', 'Type'])
        self.tree_objects.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tree_objects.setMinimumWidth(200)
        self.tree_objects.setMaximumWidth(400)
        self.tree_objects.itemSelectionChanged.connect(self.object_selected)
        # self.lbl_tree_props = QtWidgets.QLabel('Fields:')
        self.fields = QTableWidget()
        self.fields.setMinimumWidth(200)
        self.fields.setMaximumWidth(400)
        self.fields.setColumnCount(3)
        # self.fields.horizontalHeader().setStretchLastSection(True)
        self.fields.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.fields.setHorizontalHeaderLabels(['Field', 'Value', 'Offset'])
        self.fields.itemSelectionChanged.connect(self.field_selected)
        self.fields.setMaximumHeight(500)
        self.menubar = QMenuBar(self)
        menu_from_dict(self, self.menubar, MENU)
        ## eventlog
        self.eventlog = QTextBrowser()
        self.eventlog.setStyleSheet('QScrollBar:vertical {width: 10px;}')
        self.eventlog.setPlaceholderText('Log messages')
        self.eventlog.setReadOnly(1)
        rowheight = QtGui.QFontMetrics(self.eventlog.font()).lineSpacing()
        self.eventlog.setFixedHeight(10 + 4 * rowheight)
        self.eventlog.setOpenExternalLinks(True)
        self.eventlog.setFocus()
        ## status bar
        self.statusbar = QStatusBar(self)
        ## information tab
        self.info_tab = QWidget()
        self.files_label = QLabel('Files/buffers:')
        self.files = QTreeWidget()
        self.files.setColumnCount(6)
        self.files.setHeaderLabels(['ID', 'Filename', 'Size', 'MD5', 'SHA-1'])
        self.files.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.files.itemSelectionChanged.connect(self.file_selected)
        self.files.setAcceptDrops(True)
        self.files.dropEvent = self.drag_drop_received
        self.saveas_folder_label = QLabel('Save to folder:')
        self.saveas_folder = QLineEdit()
        self.saveas_folder.textEdited.connect(lambda x:self.saveas_keep_folder.add(1))
        self.saveas_browse = QPushButton('Browse', clicked=self.saveas_browse_clicked)
        self.saveas_save = QPushButton('Save', clicked=self.saveas_save_clicked)
        self.saveas_keep_folder = set()
        self.info_label = QLabel('Info:')
        self.info = QTextBrowser(styleSheet=INFO_CSS)
        self.info.anchorClicked.connect(self.dataop_link_clicked)
        self.info.setOpenLinks(False)   # todo: allow external links?
        self.info.setHtml('<html><head><title>AnTo</title></head><body></body></html>')
        self.info_ext_parse = QPushButton('Extended Parse', clicked=self.info_ext_parse_clicked)
        # self.info_doc = QtWidgets.QTextDocument()
        # self.info.setDocument(self.info_doc)
        self.dataop_op = QtWidgets.QComboBox()
        self.dataop_op_label = QLabel('Operation:')
        self.dataop_op_run = QPushButton('&Run (F5)', shortcut='F5', clicked=self.run_data_op)
        self.dataop_input = QtWidgets.QTextEdit()
        self.dataop_input.insertFromMimeData = self.dataop_input_from_mimedata
        self.dataop_input.setAcceptRichText(False)
        self.dataop_input.setFont(self.fonts['fixed'])
        self.dataop_input_label = QLabel('Input:')
        self.dataop_in_is_hex = QCheckBox('&Is hex')
        self.dataop_op_from_selection = QPushButton('From &selection', clicked=self.copy_opin_from_selection)
        self.dataop_output = QTextBrowser()
        self.dataop_output.setFont(self.fonts['fixed'])
        self.dataop_output.anchorClicked.connect(self.dataop_link_clicked)
        self.dataop_output.setOpenLinks(False)
        self.dataop_output_label = QLabel('Output:')
        self.dataop_out_is_hex = QCheckBox('Is hex')
        self.dataop_out_is_hex.setEnabled(False)
        self.dataop_op_exchange = QPushButton('▲ &Exchange ▼', clicked=self.exchange_in_out)
        self.dataop_op_out_copy_raw = QPushButton('Copy Raw', clicked=self.output_copy_raw)
        self.dataop_op_out_copy_hex = QPushButton('Copy Hex', clicked=self.output_copy_hex)
        self.dataop_add_buffer = QPushButton('Save buffer', clicked=self.dataop_add_buffer_clicked)
        self.dataop_fileop_label = QLabel('File ops:')
        ## pcap tab
        self.pcap_tab = QWidget()
        self.pcap_tab_lay, self.wmap = create_layouts(
            [
                {
                    'pcap_filename': VLabelEdit('PCAP:'),
                    'browse_pcap': QPushButton('Browse', clicked=self.clicked_browse_pcap),
                    'load_pcap': QPushButton('Load PCAP', clicked=self.clicked_load_pcap),
                },
                {
                    'pcap_packets': VTable(header=('TS', 'TopProto', 'SrcAddr', 'SrcPort', 'DstAddr', 'DstPort', 'Text')),
                },
                {
                    'pcap_filter': VLabelEdit('Filter:'),
                    'pcap_apply_filter': QPushButton('Apply', clicked=self.clicked_pcap_apply_filter),
                },
                {
                    'pcap_extract_streams': QPushButton('Extract Streams ▼', clicked=self.clicked_pcap_extract_streams),
                },
                {
                    'pcap_streams': VTable(header=('Name', 'Proto', 'Length', 'Tags', 'Text')),
                },
                {
                    'pcap_stream_filter': VLabelEdit('Filter:'),
                    'pcap_apply_stream_filter': QPushButton('Apply', clicked=self.clicked_pcap_apply_stream_filter),
                },
            ]
        )
        self.wmap = AttrDict(self.wmap)
        self.pcap_tab.setLayout(self.pcap_tab_lay)
        self.pcap_filter_model = QtCore.QSortFilterProxyModel()
        self.pcap_filter_model.setSourceModel(self.wmap.pcap_packets.dm)
        self.pcap_filter_model.setFilterKeyColumn(-1)
        self.wmap.pcap_filter.edit.returnPressed.connect(self.wmap.pcap_apply_filter.click)
        self.wmap.pcap_packets.setModel(self.pcap_filter_model)
        self.wmap.pcap_packets.selectionModel().selectionChanged.connect(self.pcap_list_sel_changed)
        self.wmap.pcap_packets.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        self.pcap_stream_filter_model = QtCore.QSortFilterProxyModel()
        self.pcap_stream_filter_model.setSourceModel(self.wmap.pcap_streams.dm)
        self.pcap_stream_filter_model.setFilterKeyColumn(-1)
        self.wmap.pcap_stream_filter.edit.returnPressed.connect(self.wmap.pcap_apply_stream_filter.click)
        self.wmap.pcap_streams.setModel(self.pcap_stream_filter_model)
        self.wmap.pcap_streams.selectionModel().selectionChanged.connect(self.pcap_streams_sel_changed)
        self.wmap.pcap_streams.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        self.config_tab = QWidget()
        self.config_tab_lay, wmap = create_layouts([
            {
                'config_dark_mode_cb': QCheckBox('Dark mode'),
            },
            {
                'config_rce_host': VLabelEdit('RCE Host:'),
                'config_rce_port': VLabelEdit('Port:', '65432'),
                'config_rce_key': VLabelEdit('Key:'),
                'config_rce_add_button': QPushButton('Add', clicked=self.add_rce_host),
                'config_rce_auto_run_remotely_cb': QCheckBox('Automatically run commands remotely if missing'),
            },
        ])
        self.wmap.update(wmap)
        self.wmap.config_dark_mode_cb.stateChanged.connect(self.dark_mode_toggled),
        self.config_tab.setLayout(self.config_tab_lay)
        ## layout components
        self.lay_main = QVBoxLayout(self.main)
        self.split_contents = QSplitter()
        self.split_contents.setOrientation(Qt.Vertical)
        self.lay_left_frame = QWidget()
        self.lay_left = QVBoxLayout(self.lay_left_frame)
        self.files_lay = QVBoxLayout()
        self.saveas_lay = QHBoxLayout()
        self.info_lay = QVBoxLayout()
        self.info_tab_lay = QVBoxLayout()
        self.info_buttons_lay = QHBoxLayout()
        self.dataop_op_lay = QHBoxLayout()
        self.dataop_tab = QWidget()
        self.dataop_lay = QVBoxLayout(self.dataop_tab)
        self.dataop_in_hdr_lay = QHBoxLayout()
        self.dataop_out_hdr_lay = QHBoxLayout()
        self.dataop_fileop_lay = QHBoxLayout()
        self.viewtabs = QTabWidget()
        self.infotabs = QTabWidget()
        self.split_h = QSplitter()
        self.split_h.setOrientation(Qt.Horizontal)
        ## layout composition
        self.lay_main.addWidget(self.menubar)
        self.lay_main.addWidget(self.split_contents)
        self.lay_main.addWidget(self.statusbar)
        self.split_contents.addWidget(self.split_h)
        self.split_contents.addWidget(self.eventlog)
        self.lay_left.addWidget(self.lbl_tree_objects)
        self.lay_left.addWidget(self.tree_objects)
        self.lay_left.addWidget(self.fields)
        self.saveas_lay.addWidget(self.saveas_folder_label)
        self.saveas_lay.addWidget(self.saveas_folder)
        self.saveas_lay.addWidget(self.saveas_browse)
        self.saveas_lay.addWidget(self.saveas_save)
        self.files_lay.addWidget(self.files_label)
        self.files_lay.addWidget(self.files)
        self.files_lay.addLayout(self.saveas_lay)
        self.info_lay.addWidget(self.info_label)
        self.info_lay.addWidget(self.info)
        self.info_lay.addLayout(self.info_buttons_lay)
        self.info_buttons_lay.addWidget(self.info_ext_parse)
        self.info_tab.setLayout(self.info_tab_lay)
        self.info_tab_lay.addLayout(self.files_lay, 1)
        self.info_tab_lay.addLayout(self.info_lay, 2)
        # todo: add QListWidget() with op. parameters to dataops
        self.dataop_lay.addLayout(self.dataop_op_lay)
        self.dataop_lay.addLayout(self.dataop_in_hdr_lay)
        self.dataop_lay.addWidget(self.dataop_input)
        self.dataop_lay.addLayout(self.dataop_out_hdr_lay)
        self.dataop_lay.addWidget(self.dataop_output)
        self.dataop_lay.addLayout(self.dataop_fileop_lay)
        self.dataop_in_hdr_lay.addWidget(self.dataop_input_label)
        self.dataop_in_hdr_lay.addWidget(self.dataop_in_is_hex)
        self.dataop_in_hdr_lay.addWidget(self.dataop_op_from_selection)
        self.dataop_out_hdr_lay.addWidget(self.dataop_output_label)
        self.dataop_out_hdr_lay.addWidget(self.dataop_out_is_hex)
        self.dataop_out_hdr_lay.addWidget(self.dataop_op_out_copy_raw)
        self.dataop_out_hdr_lay.addWidget(self.dataop_op_out_copy_hex)
        self.dataop_out_hdr_lay.addWidget(self.dataop_op_exchange)
        self.dataop_fileop_lay.addWidget(self.dataop_add_buffer)
        self.dataop_fileop_lay.addWidget(self.dataop_fileop_label)
        self.dataop_op_lay.addWidget(self.dataop_op_label)
        self.dataop_op_lay.addWidget(self.dataop_op)
        self.dataop_op_lay.addWidget(self.dataop_op_run)
        self.viewtabs.addTab(self.hexview, 'Hex')
        self.infotabs.addTab(self.info_tab, 'Information')
        self.infotabs.addTab(self.dataop_tab, 'Data processing (Ctrl+P)')
        self.infotabs.addTab(self.pcap_tab, 'PCAP')
        self.infotabs.addTab(self.config_tab, 'Configuration')
        ## splits
        self.split_h.addWidget(self.lay_left_frame)
        self.split_h.addWidget(self.viewtabs)
        self.split_h.addWidget(self.infotabs)
        ## drag & drop
        self.setAcceptDrops(True)
        self.dropEvent = self.drag_drop_received
        self.dragEnterEvent = self.drag_entered
        ## misc
        self.set_ui_text_sig.connect(self.set_ui_text_slot)
        self.connect(QtGui.QShortcut(QtGui.QKeySequence("Esc"), self), SIGNAL('activated()'), self.quit)
        self.connect(QtGui.QShortcut(QtGui.QKeySequence("Ctrl+P"), self), SIGNAL('activated()'),
                lambda:self.infotabs.setCurrentWidget(self.dataop_tab))
        self.sel2buffer = QtWidgets.QPushButton('Extract')
        self.sel2buffer.clicked.connect(self.sel2buffer_clicked)
        self.hexview.statusbar.addWidget(self.sel2buffer)


    def quit(self):
        """
        Quit.
        """
        self.hexview.close()
        CFG.ui['window'] = to_hex(self.saveGeometry().data(), sep='', wrap=0)
        CFG.ui['rce_host'] = self.wmap.config_rce_host.text()
        CFG.ui['pcap_filename'] = self.wmap.pcap_filename.text()
        CFG.ui['dark_mode'] = self.wmap.config_dark_mode_cb.isChecked()
        save_config()
        QtCore.QCoreApplication.exit()


    def apply_config(self):
        """
        Apply settings from anto.cfg.
        """
        if 'window' in CFG.ui:
            self.restoreGeometry(QtCore.QByteArray(from_hex(CFG.ui.window)))
        if 'recent_files' in CFG.ui:
            self.recent_files = CFG.ui.recent_files
            for i, e in enumerate(self.recent_files):
                self.menus[RECENT_FILES].addAction(e, functools.partial(self.api.load, None, e))
            if self.recent_files:
                action = QtGui.QAction(f'Open {self.recent_files[-1]}...', self,
                        triggered=lambda:self.api.load(None, self.recent_files[-1]), shortcut='Alt+F2')
                self.menus['&File'].insertAction(self.menus[RECENT_FILES].menuAction(), action)
        else:
            self.recent_files = CFG.ui['recent_files'] = []
        self.wmap.config_rce_host.setText(CFG.ui.get('rce_host', ''))
        self.wmap.config_dark_mode_cb.setChecked(CFG.ui.get('dark_mode', False))
        self.wmap.pcap_filename.setText(CFG.ui.get('pcap_filename', ''))


    def open(self, data, metadata=None, select=True):
        """
        Open a DataView.
        """
        if type(data) is int:
            data = self.open_files[data]
        filename = data.filename
        if filename not in self.recent_files and not data.virtual:
            menubar = self.menuBar()
            if len(self.recent_files) >= 3:
                act = list(self.menus[RECENT_FILES].actions())[0]
                menubar.removeAction(act)
                act.deleteLater()
                self.recent_files.pop(0)
            self.recent_files.append(filename)
            self.menus[RECENT_FILES].addAction(QAction(filename, self, triggered=lambda:self.api.load(None, filename)))
        if data.buffer_id not in self.open_files:
            self.open_files[data.buffer_id] = data
            node = QtWidgets.QTreeWidgetItem(self.files)
            data.gui_index = self.files.indexFromItem(node)  # todo: keep .gui_index updated when closing files
            node.setText(0, str(data.buffer_id))
            node.setText(1, ('  ' + os.path.basename(data.filename)) if data.virtual else data.filename)
            node.setText(2, str(len(data)))
            node.setText(3, data.hashes['md5'])
            node.setText(4, data.hashes['sha1'])
            node.dataview = data
            self.files.resizeColumnToContents(0)
            self.files.resizeColumnToContents(1)
            self.files.resizeColumnToContents(2)
            self.files.resizeColumnToContents(3)
            self.files.resizeColumnToContents(4)
        if select:
            self.api.objmgr.set_gui_buffer(data.buffer_id)
            self.dataview = data
            self.files.setCurrentItem(self.files.itemFromIndex(data.gui_index))
            self.hexview.open(data)
            self.hexview.jump()
            if data.filename and not self.saveas_keep_folder:
                path = os.path.dirname(data.fullname)
                self.saveas_folder.setText(path)
        if metadata:
            ...


    def saveas_browse_clicked(self):
        path = QtWidgets.QFileDialog.getExistingDirectory(self, 'Select path', self.saveas_folder.text())
        if path:
            self.saveas_folder.setText(path.rstrip('\\/') + '/')
            self.saveas_keep_folder.add(1)


    def saveas_save_clicked(self):
        filename = os.path.join(self.saveas_folder.text(), os.path.basename(self.dataview.filename))
        self.log(f'Saving {len(self.dataview)} bytes to {filename}...')
        open(filename, 'wb').write(self.dataview.raw)


    def info_ext_parse_clicked(self):
        self.api.call_extended_parse(self.dataview)


    def dataop_add_buffer_clicked(self):
        self.api.load(parent=None, filename='DataOp Output', data=self.output_data(), virtual=True)


    def dark_mode_toggled(self):
        css = DARK_CSS if self.wmap.config_dark_mode_cb.isChecked() else ''
        self.setStyleSheet(css)


    def add_rce_host(self):
        host = self.wmap.config_rce_host.text()
        port = int(self.wmap.config_rce_port.text())
        key = self.wmap.config_rce_key.text()
        runner.add_rce_host(host, port, key)
        runner.AUTO_RUN_REMOTELY = True
        self.wmap.config_rce_auto_run_remotely_cb.setChecked(True)
        self.log('RCE host added.')


    def clicked_browse_pcap(self):
        fn = QtWidgets.QFileDialog.getOpenFileName(self, "Open PCAP", None, "PCAP Files (*.pcap)")
        if fn and os.path.isfile(fn[0]):
            self.wmap.pcap_filename.setText(fn[0])


    def clicked_load_pcap(self):
        if not os.path.isfile(filename := self.wmap.pcap_filename.text()):
            return self.err('No PCAP file selected!')
        self.pcap = PCap(filename)
        self.pcap_packet_data = rows = []
        self.pcap_dv = self.api.load(parent=None, filename=filename)
        for p in self.pcap.packets:
            top = p
            l = [None, None, None]
            for i in range(3):
                l.insert(i + 1, top)
                if top.next:
                    top = top.next
                else:
                    break
            p.top = top
            p.addr_layer = l[2] or l[1]
            p.top_proto = PROTOCOLS.get(top.proto, (str(top.proto),))[0]
            rows.append((time.strftime("%y.%m.%d,%H:%M:%S", time.gmtime(p.ts)) + f'.{p.ts_us // 1000:3d}',
                    p.top_proto,
                    p.addr_layer.src, str(l[3].src if l[3] else '-'),
                    p.addr_layer.dst, str(l[3].dst if l[3] else '-'),
                    b''.join(re.findall(b'[\w_.?!;:@$^&*()\[\]{}-]+', p.top.data[p.hsize:] or b'')).decode(),
                    ))
        self.wmap.pcap_packets.setData(rows)


    def pcap_list_sel_changed(self, new, old):
        cell = new.first()
        x, y = cell.left(), cell.top()
        p = self.pcap.packets[y]
        field = [None, 'top', 'addr', 'port', 'addr', 'port', None, None]
        if not hasattr(p, 'dv'):
            p.dv = self.api.load(parent=self.pcap_dv,
                    filename=f'packet#{p.num}~{p.top_proto}~{min(p.top.src, p.top.dst)}',
                    data=p.data, virtual=True, relation='packet')
        else:
            self.open(p.dv)


    def clicked_pcap_apply_filter(self):
        filter = self.wmap.pcap_filter.text()
        self.pcap_filter_model.setFilterRegularExpression(filter)


    def clicked_pcap_extract_streams(self):
        if not self.pcap:
            return self.err('No PCAP loaded!')
        self.streams = list(extract_streams_from_pcap(self.pcap).values())
        self.stream_data = [[s.name, s.proto, str(len(s.data)), '',
                b''.join(re.findall(b'[\w_.?!;:@$^&*()\[\]{}-]+', s.data or b'')).decode()[:256],
                ] for s in self.streams]
        for i, s in enumerate(self.streams):
            ftype = run_file(s.data, brief=True)
            self.stream_data[i][3] = ftype
        self.wmap.pcap_streams.setData(self.stream_data)


    def clicked_pcap_apply_stream_filter(self):
        filter = self.wmap.pcap_stream_filter.text()
        self.pcap_stream_filter_model.setFilterRegularExpression(filter)


    def pcap_streams_sel_changed(self, new, old):
        cell = new.first()
        x, y = cell.left(), cell.top()
        s = self.streams[y]
        if not hasattr(s, 'dv'):
            s.dv = self.api.load(parent=self.pcap_dv, filename=f'stream-{s.name}',
                    data=s.data, virtual=True, relation='stream')
        else:
            self.open(s.dv)


    def sel2buffer_clicked(self):
        res = []
        s0 = 0x100000000000000
        for start, count in self.hexview.get_selections():
            s0 = min(s0, start)
            res.append(self.dataview[start:start + count])
        res = b''.join(res)
        self.api.load(parent=None, filename=f'Selection@0x{s0:X}[{len(res)}]', data=res, virtual=True)


    def drag_entered(self, e):
        """
        A drag&drop event started.
        """
        mime = e.mimeData()
        if mime.hasUrls() and [url for url in mime.urls() if url.isLocalFile()]:
            return e.accept()
        e.ignore()


    def drag_drop_received(self, e):
        """
        A drag&drop event is handled.
        """
        any = 0
        for url in e.mimeData().urls():
            if url.isLocalFile():
                filename = url.toLocalFile()
                self.api.load(None, filename)
                any = 1
        e.accept() if any else e.ignore()


    def add_data_op(self, name, is_fileop=False):
        """
        Add a new data operation.
        """
        self.dataop_op.addItem(name)
        if is_fileop:
            fop = QtWidgets.QPushButton(name, clicked=lambda:self.run_data_op(name))
            self.dataop_fileop_lay.addWidget(fop)


    def run_data_op(self, op=None):
        """
        Run data op.
        """
        if not op:
            op = self.dataop_op.currentText()
            input = self.input_data('bin')
        else:
            # if op is given, assume it's a file op
            input = None
        plg = self.api.dataops[op]
        try:
            result = plg.run(input)
        except Exception as e:
            cls, _, tb = sys.exc_info()
            frmsum = traceback.extract_tb(tb)[-1]
            self.err(f'Exception: <b>[{cls.__name__}] {e}</b> <font color="#333"> at {frmsum.filename}:{frmsum.lineno}:</font> {frmsum.line}')
            return
        self.set_output_data(result, as_html=plg.__class__.HTML)


    def copy_opin_from_selection(self):
        """
        Copy dataop input from selection.
        """
        res = []
        for start, count in self.hexview.get_selections():
            res.append(self.dataview[start:start + count])
        self.set_input_data(b''.join(res))


    def exchange_in_out(self):
        """
        Exchange input and output buffers.
        """
        old_in_text = self.dataop_input.toPlainText()
        old_in_hex = self.dataop_in_is_hex.isChecked()
        self.dataop_input.setText(self.dataop_output.toPlainText())
        self.dataop_in_is_hex.setChecked(self.dataop_out_is_hex.isChecked())
        self.dataop_output.setText(old_in_text)
        self.dataop_out_is_hex.setChecked(old_in_hex)


    def _format_data(self, data, is_hex, format):
        if is_hex:
            data = self.api.from_hex(data)
            if format == 'text' or (format == 'auto' and not self.api.is_binary(data)):
                data = data.decode('ascii', errors='replace')
        elif format in ('bin', 'hex'):
            data = data.encode('ascii')
        if format == 'hex':
            data = self.api.to_hex(data, wrap=80)
        return data


    def input_data(self, format='bin'):
        """
        Returns the DataOp input data as bytes, str or hex-encoded str.

        :param format: 'bin', 'text', 'hex' or 'auto'.
        """
        return self._format_data(self.dataop_input.toPlainText(), self.dataop_in_is_hex.isChecked(), format)


    def output_data(self, format='bin'):
        """
        Returns the DataOp output data as bytes, str or hex-encoded str.

        :param format: 'bin', 'text', 'hex' or 'auto'.
        """
        return self._format_data(self.dataop_output.toPlainText(), self.dataop_out_is_hex.isChecked(), format)


    def _set_dataop_data(self, data, ishexchk, textbox, force_hex, as_html=False):
        if as_html:
            if isinstance(data, bytes):
                data = data.decode('utf8', errors='replace')
            ishexchk.setChecked(False)
            textbox.setHtml(data)
            return
        if isinstance(data, bytes):
            is_binary = self.api.is_binary(data)
            if is_binary:
                data = self.api.to_hex(data)
            else:
                data = data.decode('ascii')
        else:
            is_binary = False
        if force_hex and not is_binary:
            data = self.api.to_hex(data)
            is_binary = True
        ishexchk.setChecked(is_binary)
        # setPlainText() makes all the text a link if setHtml() was used previously, so reset format
        textbox.setCurrentCharFormat(self.dataop_input.currentCharFormat())
        textbox.setText(data)


    def set_input_data(self, data, force_hex=False):
        """
        Set DataOp nput data.
        """
        self._set_dataop_data(data, self.dataop_in_is_hex, self.dataop_input, force_hex)


    def set_output_data(self, data, force_hex=False, as_html=False):
        """
        Set DataOp output data.
        """
        self._set_dataop_data(data, self.dataop_out_is_hex, self.dataop_output, force_hex, as_html)


    def output_copy_raw(self):
        """
        Copy output buffer to clipboard (raw).
        """
        data = self.output_data('auto')
        if type(data) is str:
            self.app.clipboard().setText(data)
        else:
            mime = QMimeData()
            mime.setData(self.api.lib.MIME_BINARY, data)
            self.app.clipboard().setMimeData(mime)


    def output_copy_hex(self):
        """
        Copy output buffer to clipboard (hex-encoded).
        """
        self.app.clipboard().setText(self.output_data('hex'))


    def dataop_input_from_mimedata(self, mime):
        """
        Hook paste.
        """
        data = None
        if mime.hasFormat(self.api.lib.MIME_BINARY):
            data = mime.data(self.api.lib.MIME_BINARY).data()
            self.set_input_data(data)
        elif mime.hasText():
            # use this to allow paste at cursor (as opposed to overwriting the whole textbox)
            getattr(QtWidgets.QTextEdit, 'insertFromMimeData')(self.dataop_input, mime)
        else:
            self.api.warn(f'Unknown formats available in clipboard: {",".join(mime.formats())}')


    def dataop_link_clicked(self, url):
        """
        A link was clicked in the dataop output.
        """
        scheme = url.scheme()
        if scheme == 'anto':
            op = url.host()
            if op == 'jump':
                args = [x for x in url.path().split('/') if x.strip()]
                buffer_id, offset = args[:2]
                if buffer_id != '*':
                    buffer_id = int(buffer_id)
                    if int(buffer_id) != int(self.files.currentItem().text(0)):
                        self.open(buffer_id)
                offset = int(offset) if offset != 'None' else None
                if offset is not None:
                    self.hexview.jump(offset)
                    if len(args) > 2 and args[2] != 'None':
                        count = int(args[2])
                        self.hexview.select_range(offset, count)
            else:
                err(f'Unknown internal link operation: {op}!')
        else:
            err(f'Unknown internal link scheme: {scheme}!')


    def file_selected(self):
        """
        The current file has changed.
        """
        self.open(self.files.currentItem().dataview)


    def object_selected(self):
        """
        A new object has been selected in the tree.
        """
        obj = self.api.get_object(self.tree_objects.currentItem().an_id)
        if obj and obj.fields:
            if (not self.files.currentItem()) or obj.data.buffer_id != int(self.files.currentItem().text(0)):
                self.open(obj.data.buffer_id)
            if obj.size:
                self.hexview.select_range(obj.offset, min(1024, obj.size))
            elif obj.offset is not None:
                self.hexview.jump(obj.offset)
            self.fields.setRowCount(len(obj.fields))
            for row, (name, field) in enumerate(obj.fields.items()):
                self.fields.setItem(row, 0, QTableWidgetItem(field.name))
                value = field.value
                if isinstance(value, bytes):
                    value = self.api.to_hex(value, truncate=20)
                elif isinstance(value, int):
                    value = str(value) if value < 10 else (f'0x{value:X} ({value})' if value < 0x100000000 else f'0x{value:X}')
                self.fields.setItem(row, 1, QTableWidgetItem(value if (isinstance(value, int) or isinstance(value,str)) else ''))
                self.fields.setItem(row, 2, QTableWidgetItem(f'0x{field.offset:X}' if field.offset is not None else ''))
                hint = getattr(field, 'hint', getattr(field, 'description', None))
                if hint:
                    self.setToolTip(hint)
            self.fields.resizeRowsToContents()
            self.fields.resizeColumnsToContents()
        else:
            self.fields.setRowCount(0)


    def field_selected(self):
        """
        A new field has been selected in the tree.
        """
        obj = self.api.get_object(self.tree_objects.currentItem().an_id)
        if obj and obj.fields:
            field = list(obj.fields.values())[self.fields.currentRow()]
            if field.offset is not None and field.size is not None:
                self.hexview.select_range(field.offset, min(256, field.size))


    @Slot(str, str)
    def set_ui_text_slot(self, node, value):
        """
        Slot for the .set_ui_text_sig() signal.
        """
        if node == 'eventlog':
            # if self.cfg.log_timestamps:
            if value.startswith('[DEBUG]'):
                value = f"<font color='#444'>{value}</font>"
            elif value.startswith('[ERROR]'):
                value = f"<font color='#600'>{value}</font>"
            value = f"<font color='#432'>{time.strftime('%H:%M:%S')}</font> {value}"
            self.eventlog.append(value)
            self.eventlog.ensureCursorVisible()


    def set_ui_text(self, node, value):
        """
        Allows setting UI elements from other threads (signal/slot mechanism).
        """
        value = str(value)
        self.set_ui_text_sig.emit(node, value)


    def update_info(self, section=None):
        """
        Update the displayed information when new items are added.
        """
        data = self.info.toHtml()
        data = re.sub('<p style=.+?</p>', '', data)     # fix empty para persistently inserted by Qt
        if INFO_CSS not in data:
            data = data.replace('</head>', f'<style>\n{INFO_CSS}\n</style></head>')
        # todo: better section update (the rx below sometimes fails)
        data = '\n'.join((f'<h2>{sec}</h2>\n' + self.api.data_to_html(self.api.info[sec], heading=3)) for sec, info in self.api.info.items())
        """
        data, count = re.subn(f'<!--SECTION:{section}-->.+?<!--/SECTION-->',
                sec_data.replace('\\', '\\\\'), data, flags=re.S)
        if not count:
            data = data.replace('</body>', f'<!--SECTION:{section}-->\n{sec_data}\n<!--/SECTION-->\n</body>')
        """
        self.info.setHtml(data)


    def log(self, msg):
        """
        Add message to the log.
        """
        self.set_ui_text('eventlog', msg)


    def dbg(self, msg):
        """
        Add a debug message to the log.
        """
        self.set_ui_text('eventlog', '[DEBUG] ' + msg)


    def err(self, msg):
        """
        Add an error message to the log.
        """
        self.set_ui_text('eventlog', '[ERROR] ' + msg)


    def add_tree_node(self, obj):
        """
        Adds an object to the object tree.
        """
        toplevel = obj.parent == None
        node = obj.ui_object = QtWidgets.QTreeWidgetItem(self.tree_objects if toplevel else obj.parent.ui_object)
        node.setText(0, obj.name)
        node.setText(1, f'0x{obj.offset:X}' if obj.offset is not None else '')
        node.setText(2, obj.obj_type)
        if obj.depth < 3 and len(obj.children) < 10:
            node.setExpanded(True)
        self.tree_objects.resizeColumnToContents(0)
        self.tree_objects.resizeColumnToContents(1)
        self.tree_objects.resizeColumnToContents(2)
        return node


    def menu_Open(self):
        fn = QtWidgets.QFileDialog.getOpenFileName(self, "Open File", None, "Any Files (*.*)")
        if fn and os.path.isfile(fn[0]):
            self.api.load(None, fn[0])


    def menu_Quit(self):
        self.close()


def run_ui():
    app = QtWidgets.QApplication()
    app.setApplicationName('anto')
    window = AntoMain(app)
    window.show()
    return window
