"""
Pure-Python PySide6 (Qt5) hex viewer / editor widget.

Author: Vlad Topan (vtopan/gmail)
"""

from ..core import DataView

from PySide6 import QtGui, QtWidgets, QtCore
from PySide6.QtCore import Qt, SIGNAL, QItemSelectionModel

import ast
import re
import sys
from threading import Thread
import time


VER = '0.1.2 (2019.10.20)'


NONPRINTABLE = (0, 7, 8, 9, 10, 13)
HEADER_CSS = '''
::section {
    border: 0px;
    padding: 1px;
    text-align: center;
    font: monospace;
    }
::section:checked {
    color: white;
    font-weight: normal;
    background-color: blue;
    }
::section:hover {
    color: blue;
    }
'''

TABLE_CSS = '''
::item {
    gridline-color: white;
    }
::item:focus {
    border: 0px;
    color: red;
    gridline-color: white;
    }
::item:hover {
    color: blue;
    }
'''



class QHexEditor(QtWidgets.QWidget):
    """
    Hex editor widget.

    :param data: The raw data (bytes or bytearray).
    :param filename: The file name containing the data.
    """

    def __init__(self, data=None, filename=None, readonly=True, statusbar=1, columns=16, font=None):
        super().__init__()
        self.font = font or QtGui.QFont('Terminal', 8)
        self.metrics = QtGui.QFontMetrics(self.font)
        self.row_height = self.metrics.height() + 4     # fixme: this is WAY off due to huge padding
        self.columns = columns
        self.rows = 1
        self.perpage = self.columns * self.rows
        self.dm = None
        self.hexview = QtWidgets.QTableView(font=self.font, showGrid=0, styleSheet=TABLE_CSS)
        self.hexview.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Fixed)
        self.hexview.horizontalHeader().setHighlightSections(1)
        self.hexview.verticalHeader().setHighlightSections(1)
        self.hexview.verticalHeader().setMaximumSectionSize(self.row_height)
        self.scroll = QtWidgets.QScrollBar(Qt.Vertical)
        self.scroll.setPageStep(1)
        self.scroll.valueChanged[int].connect(self.jump_to_row)
        self.l1 = QtWidgets.QVBoxLayout()
        self.l2 = QtWidgets.QHBoxLayout()
        self.l2.addWidget(self.hexview)
        self.l2.addWidget(self.scroll)
        self.l1.addLayout(self.l2)
        self.statusbar = statusbar and QtWidgets.QStatusBar()
        if self.statusbar:
            self.l1.addWidget(self.statusbar)
            self.status = {}
            self.status['filename'] = QtWidgets.QLabel('-')
            self.lbl_offset = QtWidgets.QLabel('Offset:')
            self.status['offset'] = QtWidgets.QLabel('-')
            self.lbl_selection = QtWidgets.QLabel('Selection:')
            self.status['selection'] = QtWidgets.QLabel('-')
            self.statusbar.addWidget(self.status['filename'])
            self.statusbar.addWidget(self.lbl_offset)
            self.statusbar.addWidget(self.status['offset'])
            self.statusbar.addWidget(self.lbl_selection)
            self.statusbar.addWidget(self.status['selection'])
            self.statusbar.addWidget(QtWidgets.QLabel('Jump:'))
            self.jump_box = QtWidgets.QLineEdit()
            self.statusbar.addWidget(self.jump_box)
            self.jump_box.returnPressed.connect(self.jump_box_enter_pressed)
            self.jump_box.setMaximumWidth(80)
        self.setLayout(self.l1)
        self.setMinimumWidth(800)
        self._data = None
        self.dm = QtGui.QStandardItemModel(self.rows, self.columns)
        self.hexview.setModel(self.dm)
        self.hexview.horizontalHeader().setStretchLastSection(0)
        self.hexview.selectionModel().selectionChanged.connect(self.sel_changed)
        # self.hexview.clicked.connect(self.click)
        self.hexview.mousePressEvent = self.mouse_press
        self.column_labels = [hex(x)[2:].upper() for x in range(self.columns)] * 2
        self.open(data, filename, readonly)
        self.draw_iteration = 0
        self.brushes = [QtGui.QBrush(QtGui.QColor(x)) for x in ('#800', '#008')]
        self._view_params = None
        self.update_view()
        self.jump()
        self.alt_sel = [None, None]


    def close(self):
        """
        Explicitly close relevant objects.
        """
        ...


    def jump_box_enter_pressed(self):
        text = self.jump_box.text()
        value = ast.literal_eval(re.sub(r'^([a-fA-F0-9]+)h$', r'0x\1', text))
        self.jump_box.setText(f'0x{value:X}')
        # self.jump(value)
        self.select_range(value, 1)


    def sel_changed(self, new, old):
        """
        Selection changing is monitored to allow highlighting matching selected bytes between the hex dump and the text.
        """
        new, old = ([(e.row(), (e.column() + self.columns) % (2 * self.columns)) for e in lst.indexes()] \
                for lst in (new, old))
        for e in old:
            if e in new:
                continue
            item = self.dm.item(*e)
            if item:
                item.setBackground(QtGui.QColor('#FFFFFF'))
        for e in new:
            if e in old:
                continue
            item = self.dm.item(*e)
            if item:
                item.setBackground(QtGui.QColor('#DDDD55'))
        if self.statusbar:
            idx = self.hexview.selectionModel().currentIndex()
            offs = self.offs + idx.row() * self.columns + idx.column()
            self.status['offset'].setText('<b>0x%X</b> (%d)' % (offs, offs))
            sels = self.get_selections()
            sel = '-'
            if sels:
                total = sum(e[1] for e in sels)
                if total > 1:
                    sel = f'<b>0x{sels[0][0]:X}</b>..<b>0x{sels[-1][0] + sels[-1][1] - 1:X}</b> ({total} bytes)'
            self.status['selection'].setText(sel)


    def select_range(self, start, count, jump=True):
        """
        Select a range of bytes.
        """
        base_row = max(0, (start // self.columns) - 1)
        if jump:
            self.jump_to_row(base_row)
        base_row = self.offs // self.columns
        selmdl = self.hexview.selectionModel()
        mdl = self.hexview.model()
        selmdl.clearSelection()
        max_rows = self.rows
        while count > 0 and max_rows >= 0:
            row = start // self.columns - base_row
            col1 = start % self.columns
            col2 = min(self.columns - 1, col1 + count - 1)
            selected = col2 - col1 + 1
            count -= selected
            start += selected
            # todo: use row selection when support is added
            #if selected == self.columns:
            #    selmdl.select(mdl.index(row, 0), QItemSelectionModel.Select | QItemSelectionModel.Rows)
            #else:
            selmdl.select(QtCore.QItemSelection(mdl.index(row, col1), mdl.index(row, col2)), QItemSelectionModel.Select)
            max_rows -= 1


    def get_selections(self):
        """
        Returns a list of tuples describing selection ranges: (start, count).
        """
        selmdl = self.hexview.selectionModel()
        mdl = self.hexview.model()
        res = []
        for sel in selmdl.selection():
            for row in range(sel.top(), sel.bottom() + 1):
                res.append((self.offs + row * self.columns + sel.left(), sel.right() - sel.left() + 1))
        return res


    def wheelEvent(self, evt):
        """
        Hooked to implement scrolling by mouse wheel.
        """
        self.jump_rows(1 if evt.angleDelta().y() > 0 else -1)
        evt.accept()


    def mouse_press(self, event):
        if event.type() == QtCore.QEvent.MouseButtonPress and QtGui.QGuiApplication.keyboardModifiers() == QtCore.Qt.AltModifier:
            idx = self.hexview.indexAt(event.pos())
            if idx.isValid():
                x, y = idx.column(), idx.row()
                if x >= 16:
                    x -= 16
                offs = self.offs + y * self.columns + x
                self.alt_sel[0 if event.button() == QtCore.Qt.LeftButton else 1] = offs
                if self.alt_sel[0] is not None and self.alt_sel[1] is not None and self.alt_sel[0] <= self.alt_sel[1]:
                    self.select_range(self.alt_sel[0], self.alt_sel[1] - self.alt_sel[0] + 1)
                event.accept()
                return
        super(QtWidgets.QTableView, self.hexview).mousePressEvent(event)


    def keyPressEvent(self, evt):
        """
        Hooked to implement scrolling by up/down/pgup/pgdn.

        Note: only events which aren't caught by self.hexview end up here.
        """
        key = evt.key()
        if key in (Qt.Key_PageUp, Qt.Key_PageDown):
            self.jump_pages(1 if key == Qt.Key_PageUp else -1)
            evt.accept()
        elif key in (Qt.Key_Up, Qt.Key_Down):
            self.jump_rows(1 if key == Qt.Key_Up else -1)
            evt.accept()


    def resizeEvent(self, evt):
        """
        This keeps self.rows/columns/perpage updated and regenerates the self.hexdump content.
        """
        super().resizeEvent(evt)
        self.update_view()
        self.scroll.setPageStep(self.rows)
        self.jump(self.offs)


    def update_view(self):
        """
        Update view settings based on rows / columns / window size.
        """
        vp = (self.rows, self.columns, self.hexview.height(), self.row_height)
        if vp == self._view_params:
            return
        self._view_params = vp
        self.rows = (self.hexview.height() - self.hexview.horizontalHeader().height() - 6) // self.row_height
        self.perpage = self.columns * self.rows
        self.dm.setRowCount(self.rows)
        ### format view
        hv = self.hexview
        vh = hv.verticalHeader()
        hh = hv.horizontalHeader()
        vh.setStyleSheet(HEADER_CSS)
        hh.setStyleSheet(HEADER_CSS)
        hh.setMaximumSectionSize(hv.fontMetrics().horizontalAdvance('AA') + 6)
        hh.setMinimumSectionSize(hv.fontMetrics().horizontalAdvance('A') + 2)
        hh.setDefaultSectionSize(hv.fontMetrics().horizontalAdvance('AAA'))
        vh.setDefaultSectionSize(self.row_height)
        vh.setDefaultAlignment(Qt.AlignCenter)
        ### table items
        self.table_items = [[None] * self.columns for r in range(self.rows)]
        for row in range(self.rows):
            for col in range(self.columns):
                hitem = QtGui.QStandardItem('')
                hitem.setTextAlignment(Qt.AlignCenter)
                hitem.setForeground(self.brushes[col % 2])
                titem = QtGui.QStandardItem(' ')
                titem.setTextAlignment(Qt.AlignCenter)
                titem.setForeground(self.brushes[col % 2])
                self.table_items[row][col] = (hitem, titem)


    def open(self, data=None, filename=None, readonly=True):
        """
        Open a file (or a data buffer).
        """
        self.offs = 0
        if isinstance(data, DataView):
            self._data = data
        else:
            self._data = DataView.open(data=data, filename=filename, readonly=readonly)
        self.size = len(self._data)
        self.scroll.setRange(0, self.size // self.columns)
        if self.statusbar:
            self.status['filename'].setText((f'File: <b>{filename}</b>') \
                    if filename else (f'Raw data ({self.size} bytes)'))


    def jump_pages(self, pages=1):
        """
        Jump a number of pages up / down.

        :param pages: Number of pages to jump; negative values jump down.
        """
        self.jump(self.offs - pages * self.perpage)


    def jump_rows(self, rows=1):
        """
        Jump a number of rows up / down.

        :param rows: Number of rows to jump; negative values jump down.
        """
        self.jump(self.offs - rows * self.columns)


    def jump_to_row(self, row):
        """
        Jump to the given row id.
        """
        self.jump(row * self.columns)


    def jump(self, offs=0, force=False):
        """
        Jump to the given offset (this is the main workhorse which updates the self.hexview contents).
        """
        offs = self.offs = max(0, min(offs, self.size - self.perpage) // self.columns * self.columns)
        hv = self.hexview
        #selmodel = hv.selectionModel()
        #crt = selmodel.currentIndex()
        vh = hv.verticalHeader()
        hh = hv.horizontalHeader()
        ### get data window
        data = self._data[offs:offs + self.perpage]
        iteration = self.draw_iteration = self.draw_iteration + 1
        self.dm.clear()
        ### generate & set labels
        addrlabels = ['%08X' % i for i in range(offs, offs + self.perpage, self.columns)]
        self.dm.setVerticalHeaderLabels(addrlabels)
        self.dm.setHorizontalHeaderLabels(self.column_labels)
        if iteration < self.draw_iteration:
            return
        ### generate data for datamodel (actual hexview contents)
        idx = 0   # offset inside the data window (index in `data`)
        for row in range(self.rows):
            for col in range(self.columns):
                if iteration < self.draw_iteration:
                    return
                if idx < len(data):
                    c = data[idx]
                    hitem, titem = self.table_items[row][col]
                    hitem.setText('%02X' % c)
                    c = chr(c) if 0x20 <= c <= 0x7F else bytes([c or 0x20]).decode('cp437')
                    titem.setText(c)
                else:
                    hitem = titem = None
                if iteration < self.draw_iteration:
                    return
                self.dm.setItem(row, col, hitem)
                if iteration < self.draw_iteration:
                    return
                self.dm.setItem(row, col + self.columns, titem)
                idx += 1
        ### restore pre-scroll selection
        #hv.setCurrentIndex(crt) # todo: find a way to restore the selected area which doesn't crash...
        for i in range(self.columns):
            hh.resizeSection(i, hv.fontMetrics().horizontalAdvance('AAa'))
            hh.resizeSection(i + self.columns, hv.fontMetrics().horizontalAdvance('A'))



if __name__ == '__main__':
    # import random
    import sys
    # data = bytes(random.randint(0, 255) for i in range(1024))
    fn = sys.argv[-1]   # filename given as parameter or the script itself
    data = open(fn, 'rb').read()
    app = QtWidgets.QApplication()
    he = QHexEditor(data=data)
    he.show()
    sys.exit(app.exec_())

