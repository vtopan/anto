Place binary tools here in the appropriate platform folder (Windows / Linux) if not found in %PATH% / $PATH.

Sources:
- radare2: https://github.com/radareorg/radare2/releases
- file: install either MSYS2 (https://www.msys2.org/) or Cygwin (https://www.cygwin.com/) or extract the portable version from https://git-scm.com/download/win
