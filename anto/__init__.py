import sys
import os

PKG_PATH = os.path.dirname(__file__)
sys.path.append(f'{PKG_PATH}/external/libvstruct2')
sys.path.append(f'{PKG_PATH}/external/iolib')
sys.path.append(f'{PKG_PATH}/external/libioqt')

from .app import GuiApp

__VER__ = '0.1.0 (pre-alpha)'
