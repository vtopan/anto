"""
PE file & related data structures loaders / parsers.

VS build info sources:
- https://www.tmurgent.com/appv/en/resources/tools/167-visual-studio-runtime-versions-chart
- https://omwtm.blog/2014/12/03/visual-studio-2013-version-numbers-and-updates/
- https://docs.microsoft.com/en-us/visualstudio/install/visual-studio-build-numbers-and-release-dates?view=vs-2017
- https://docs.microsoft.com/en-us/visualstudio/install/visual-studio-build-numbers-and-release-dates?view=vs-2019
- https://github.com/agrajag9/getrichlaikaboss/blob/master/comp_ids.txt


Author: Vlad Topan (vtopan/gmail)
"""
from anto.api import AnParser, AnHookParser
from anto.core import AnObject

import re
import struct


RX = {
    'ret_or_int3': (rb'\xC2[\x02-\x20]|\xC3|.(?=\xCC)',),
    }
for k in RX:
    RX[k] = re.compile(*RX[k])


RICH_DANS_MARKER = 0x536E6144   # "DanS"
RICH_MCV_BUILD_RANGES = {
    'VS 2019 (16.x)': [(28329, 29911),],                    # from M$
    'VS 2017 (15.x)': [(26228, 28307),],                    # from M$
    'VS 2015 (14.x)': [(23026, 25431),],
    'VS 2013 (12.x)': [(21005,), (30110,), (30324,), (30501,), (30723,), (31101,), (40629,)],
    'VS 2013 or 2015 (12.x or 14.x)': [(40660, 40660),],
    'VS 2012 (11.x)': [(50728, 51106), (60315, 61219),],
    'VS 2010 (10.x)': [(30319,), (40219,)],
    'VS 2008 (9.x)': [(21022,), (30729,)],
    'VS 2005 (8.x)': [(2900,), (3790,)],
    'VS 2005 (8.x) or VS 2012 (11.x)': [(50727,)],
    'VS 2002/2003 (7.x)': [(1455, 1473), (3052, 3077), (6030,), (9466,)],
    'VS 6.0 (6.x)': [(1720, 1736), (8168, 8804),],
    'VS 97 (5.x)': [(1668,), (7303,),],
    }
for k, v in RICH_MCV_BUILD_RANGES.items():
    for i, e in enumerate(v[:]):
        if len(e) == 1:
            v[i] = (e[0], e[0])



class PE(AnParser):
    """
    AnTo extension to the libvstruct2.PE parser.
    """


    def parse(self):
        # enumerate sections
        sheaders = self.NTHeader.SectionHeaders
        ep = self.NTHeader.OptionalHeader.AddressOfEntryPoint.value
        sections = AnObject(data=self.data, parent=self, obj_type='PESections', name='Sections')
        # todo: make PE.Sections an AnInfoView displaying the sections as a table
        for i, sh in enumerate(sheaders):
            if sh.SizeOfRawData.value:
                offset, size = sh.PointerToRawData.value, sh.SizeOfRawData.value
                sec = AnObject(data=self.data, parent=sections, obj_type='PESection', name=f'Section[{i}]',
                        offset=offset, size=size)
                sec.add_field('Data', offset=offset, size=size)
                if sh.VirtualAddress.value <= ep <= sh.VirtualAddress.value + sh.VirtualSize.value:
                    sec.add_field('EntryPoint', offset=ep - sh.VirtualAddress.value + sh.PointerToRawData.value, size=32)
        if hasattr(self.vs2, 'Exports'):
            exports = AnObject('Exports', data=self.data, parent=self, obj_type='PEExports')
            for e in self.vs2.Exports.values():
                offset = e['FunctionOffset']
                if offset:
                    m = RX['ret_or_int3'].search(self.data.raw, pos=offset, endpos=min(len(self.data), offset + 2048))
                    size = m.end() - offset if m else 32
                else:
                    size = 0
                exports.add_field(e.get('Name', f'Ordinal#{e["Ordinal"]}'), offset=offset, size=size,
                        description=f'RVA:{e["FunctionRVA"]}', value='-')



class PERichHeader(AnHookParser):
    # todo: move this to PE
    STRUCT = 'PE'


    @classmethod
    def present(cls, obj):
        """
        Check if structure is present.

        :param obj: A PE() instance.
        """
        return obj.data[0x80:0x100].find(b'Rich') >= 0


    def parse(self):
        rich_offs = self.data[0x80:0x100].find(b'Rich')
        if rich_offs < 0:
            return
        rich_offs += 0x80
        read_I4 = self.data.read_I4
        xor_key = read_I4(rich_offs + 4)
        values = []
        for i in range(rich_offs - 4, 0, -4):
            dword = read_I4(i) ^ xor_key
            values.append(dword)
            if dword == RICH_DANS_MARKER:
                offs = self.offset = i
                break
        else:
            return
        values.reverse()
        self.add_field(name='DanS', value=values[0], offset=offs, size=4)
        self.add_field(name='Padding', value=struct.pack('<III', *values[1:4]), offset=offs + 4, size=12)
        values = values[4:]
        for i in range(0, len(values), 2):
            num = i >> 1
            coffs = offs + 16 + num * 8
            compid = values[i]
            ProdID, mCV = compid >> 16, compid & 0xFFFF
            count = values[i + 1]
            new_obj = AnObject(data=self.data, parent=self, obj_type='@comp.id', name=f'@comp.id[{num}]',
                    offset=coffs, size=8)
            new_obj.add_field(name=f'@comp.id', value=compid, offset=coffs, size=4)
            field = new_obj.add_field(name=f'ProdID', value=ProdID, offset=coffs, size=2)
            new_obj.add_field(name=f'mCV', value=mCV, offset=coffs + 2, size=2, description='Linker build number (e.g. the "26715" in 19.15.26715)')
            for k, v in RICH_MCV_BUILD_RANGES.items():
                for v1, v2 in v:
                    if v1 <= mCV <= v2:
                        field.hint = k
                        field = new_obj.add_field(name=f'Linker', value=k, offset=coffs + 2, size=2)
            new_obj.add_field(name=f'Count', value=count, offset=coffs + 4, size=4, description='Number of linked object files of this type/version')
        self.add_field(name='XOR-Key', value=xor_key, offset=rich_offs + 4, size=4)
        self.add_field(name='Padding', value=self.data[rich_offs + 8:rich_offs + 16], offset=rich_offs + 8, size=8)
        self.size = rich_offs - self.offset + 8
        # todo: validate checksums, warn if invalid
        # todo: move to libvstruct2

