"""
ASN.1 parsing.

Author: Vlad Topan (vtopan/gmail)
"""
from anto.core import AnObject
from anto.api import lib, AnParser, AnCStructParser, AnHookParser, err

import base64
import re
import traceback


RX = {
    'pem-cert': (rb'^-----BEGIN ([ A-Z]+)-----\s*([a-zA-Z0-9+/=\r\n]+)-----END \1-----\s*$',),
    }
for k in RX:
    RX[k] = re.compile(*RX[k])



def asn1_to_anto(obj, name, data, parent=None):
    """
    Convert an ASN1Entry container (and contents) to AnTo objects/fields.
    """
    if not obj.is_container:
        raise ValueError('asn1_to_anto() can only convert ASN.1 containers!')
    aobj = AnObject(data=data, name=name, obj_type=obj.tag_name, offset=obj.offset, size=obj.size, parent=parent)
    for i, e in enumerate(obj.value):
        fname = f'{e.tag_name}_{i:02}'
        aobj.add_field(name=fname, value=e.value if not e.is_container else None, offset=e.offset, size=e.size)
        if e.is_container and any(ee.is_container for ee in e.value):
            asn1_to_anto(e, fname, data, parent=aobj)
    return aobj


def dict_to_anto(d, name, data, parent=None):
    """
    Convert a dictionary (whose values can be lists, dicts and basic types) to AnTo objects.
    """
    if not isinstance(d, dict):
        raise ValueError(f'dict_to_anto() requires a dict (got {type(d)}: {d})!')
    aobj = AnObject(data=data, name=name, parent=parent, obj_type='{}',
            offset=getattr(d, 'offset', None), size=getattr(d, 'size', None))
    for k, v in d.items():
        if isinstance(v, dict):
            dict_to_anto(v, k, data, aobj)
        elif isinstance(v, list):
            lobj = AnObject(data=data, name=k, parent=aobj, obj_type='[]')
            for i, e in enumerate(v):
                dict_to_anto(e, f'Item#{i}', data, lobj)
        else:
            aobj.add_field(name=k, value=v if type(v) not in (list, dict) else None,
                    offset=getattr(v, 'offset', None), size=getattr(v, 'size', None))



class ASN1(AnParser):
    """
    ASN.1-encoded file.
    """
    FILTER = b'^0[\x80-\x8F]'

    def parse(self):
        """
        The actual parsing (the bytes-like data is in `self.data` at offset `self.offset`).
        """
        self.asn1 = lib.ASN1Entry(self.data)
        self.root = asn1_to_anto(self.asn1, name='Root', data=self.data, parent=self)



class DERCatalog(AnHookParser):
    """
    DER/ASN.1-encoded catalog file.
    """
    STRUCT = 'ASN1'


    @classmethod
    def present(cls, obj):
        """
        Check if structure is present.

        :param obj: A ASN1() instance.
        """
        root = obj.asn1
        return root.is_container and len(root.value) == 2 and root[0].oid_name == 'signedData' and len(root[1][0].value) == 5


    def parse(self):
        try:
            self.catalog = dict_to_anto(lib.parse_catalog(self.asn1), name='Catalog',
                    data=self.data, parent=self)
        except Exception as e:
            err(f'Failed parsing catalog: {traceback.format_exc()}!')



class DERCertificate(AnHookParser):
    """
    DER/ASN.1-encoded certificate file.
    """
    STRUCT = 'ASN1'


    @classmethod
    def present(cls, obj):
        """
        Check if structure is present.

        :param obj: A ASN1() instance.
        """
        root = obj.asn1
        return len(root) == 3 and \
            (
                (len(root[0]) == 7 and root[0][0].value == 1)
                or
                (len(root[0]) == 8 and root[0][0][0].value == 2)
            )


    def parse(self):
        try:
            self.certificate = dict_to_anto(lib.parse_certificate(self.parent.asn1),
                    name='Certificate', data=self.data, parent=self)
        except Exception as e:
            err(f'Failed parsing certificate: {traceback.format_exc()}!')



class PEMCertificate(AnParser):
    """
    PEM ("Privacy Enhanced Mail", ASCII-encoded) certificate file.
    """
    FILTER = b'^-----BEGIN [ A-Z]+-----'


    def parse(self):
        m = RX['pem-cert'].search(self.data[:32 * 1024])
        if not m:
            return err('Invalid PEM-encoded certificate!')
        ctype, b64data = m.groups()
        ctype = ctype.decode()
        self.add_info(f'PEM Certificate Type: {ctype}')
        self.add_field(name='CertType', value=ctype, offset=m.start(1), size=len(ctype))
        data = base64.b64decode(b64data)
        self.api.load_embedded(self, data, filename='Decoded ASN.1 Data.der')
