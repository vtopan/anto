import hashlib
import os

from anto.api import lib, AnInfo, AttrDict, data, log, dbg, err

from anto.plugins.dataop.yara import run_yara, format_yara_results



class AnalyzeForCTF(AnInfo):


    def parse(self):
        ...


    def extended_parse(self):
        log(f'Running YARA...')
        filename = self.data.filename and os.path.isfile(self.data.filename) and self.data.filename
        res = run_yara(filename or self.data.raw)
        if res:
            res = format_yara_results(res, self.data.buffer_id)
            self.add_info({'YARA': res.split('<br>')})

