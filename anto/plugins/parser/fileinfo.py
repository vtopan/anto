"""
Generic file information extractor.

Author: Vlad Topan (vtopan/gmail)
"""
from anto.core import AnObject
from anto.api import lib, AnInfo, AnCStructParser, AnHookParser, AttrDict, data, \
        make_internal_link
from anto.plugins.dataop.file import run_file
from anto.plugins.dataop.exiftool import run_exiftool

import hashlib
import os
import re


class FileInfo(AnInfo):
    """
    Generic file info extractor.
    """


    def parse(self):
        """
        The actual parsing (the bytes-like data is in `self.data` at offset `self.offset`).
        """
        info = self.data.info = AttrDict()
        filename = None
        if self.data.filename:
            self.add_info(f'Filename: `{make_internal_link(self.data.filename, self.data.buffer_id)}`')
            if os.path.isfile(self.data.filename):
                filename = self.data.filename
        if parent := self.data.parent:
            lnk = make_internal_link(os.path.basename(parent.filename), parent.buffer_id, -1)
            self.add_info(f'Parent (as {self.data.relation}): #{self.data.parent.buffer_id} {lnk}')
        for name in ('MD5', 'SHA-1', 'SHA-256', 'SHA-512'):
            sname = name.replace('-', '').lower()
            h = hashlib.new(sname)
            h.update(self.data.raw)
            value = h.hexdigest()
            setattr(info, sname, value)
            self.add_info(f'{name}: `{value}`')
        self.add_info(f'Size: {len(self.data)} bytes')
        self.add_info(f'File type: {data.get_file_type(self.data)}')
        res = run_file(filename or self.data.raw)
        if res:
            info.ext_file_type = res.strip()
            self.add_info(f'<b>file</b>: `{res}`')


    def extended_parse(self):
        filename = self.data.filename and os.path.isfile(self.data.filename) and self.data.filename
        res = run_exiftool(filename or self.data.raw)
        if res:
            out = [('%s: `%s`' % tuple(re.split('\s+:\s+', e, maxsplit=1))) for e in re.split(r'[\n\r]+', res) if ' : ' in e]
            self.add_info({'exiftool': out})
