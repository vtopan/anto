"""
AnTo-specific data processing.

Author: Vlad Topan (vtopan/gmail)
"""
from anto.api import AnDataOp, parse_c_to_anstruct, parse_c_to_vstruct


class An_CStruct_To_AnStruct(AnDataOp):
    INTYPE = str

    def op(self, input, source):
        res = parse_c_to_anstruct(input).strip() + '\n'
        return res


class An_CStruct_To_VStruct(AnDataOp):
    INTYPE = str

    def op(self, input, source):
        res = parse_c_to_vstruct(input).strip() + '\n'
        return res
