"""
XOR.

Author: Vlad Topan (vtopan/gmail)
"""
from anto.api import AnDataOp, from_hex, to_hex

import html
import re
import urllib.parse as up
import zlib


class An_XOR_Byte_All(AnDataOp):

    def op(self, input, source):
        res = []
        for i in range(1, 255):
            xored = bytes(e ^ i for e in input)
            as_text = re.sub(b'[^\\x20-\\x7E]+', b'', xored).decode()
            # double points for letters, digits, underscore, curly braces
            score = len(re.sub('[^a-zA-Z0-9{}_]', '', as_text)) + len(as_text)
            as_py_str = re.sub(b'[^\\x20-\\x7E]', lambda e: b'\\x%02X' % ord(e.group()),
                    xored).decode()
            res.append((i, score, as_py_str))
        res = sorted(res, key=lambda e: e[1], reverse=1)
        res = '\n'.join(f'{e[0]:02X} ({e[1]}): {e[2]}' for e in res)
        return res


# todo: XOR with fixed key (as arg, needs plugin arg implementation), 2 and 4-byte keys, etc.

