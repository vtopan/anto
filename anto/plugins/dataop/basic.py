"""
Basic data operations.

Author: Vlad Topan (vtopan/gmail)
"""
from anto.api import AnDataOp, from_hex, to_hex

import base64
import html
import urllib.parse as up
import zlib


class An_Base64_Decode(AnDataOp):
    def op(self, input, source):
        if isinstance(input, bytes):
            input = input.decode('ascii')
        return base64.b64decode(input)


class An_Base64_Encode(AnDataOp):
    def op(self, input, source):
        if isinstance(input, str):
            input = input.encode('ascii')
        return base64.b64encode(input).decode('ascii')


class An_URL_Quote(AnDataOp):
    OP = up.quote


class An_URL_Unquote(AnDataOp):
    OP = up.unquote


class An_HTML_Escape(AnDataOp):
    INTYPE = str
    OP = html.escape


class An_HTML_Unescape(AnDataOp):
    INTYPE = str
    OP = html.unescape


class An_Zlib_Compress(AnDataOp):
    OP = zlib.compress


class An_Zlib_Decompress(AnDataOp):
    OP = zlib.decompress


class An_Hex_Decode(AnDataOp):
    OP = from_hex


class An_Hex_Encode(AnDataOp):
    OP = to_hex
