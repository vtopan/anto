"""
Capstone diassembler.

Author: Vlad Topan (vtopan/gmail)
"""
from anto.api import AnDataOp, err, to_hex

try:
    import capstone
    from capstone import Cs, CS_ARCH_X86, CS_ARCH_ARM, CS_MODE_32, CS_MODE_64
except ImportError:
    err('Missing capstone disassembler library!')
    capstone = None


if capstone:

    class An_Disassemble_Capstone(AnDataOp):
        INTYPE = bytes
        ARGS = {
            'arch': ('Architecture', ('X86', 'ARM')),
            'mode': ('Bits', ('32', '64')),
            'addr': ('Address', 0),
        }


        def op(self, input, source, arch='X86', mode=32, virt_addr=0):
            if arch not in ('X86', 'ARM'):
                raise ValueError(f'Invalid Capstone arch {arch}!')
            arch = globals()[f'CS_ARCH_{arch}']
            if mode not in ('32', '64', 32, 64):
                raise ValueError(f'Invalid Capstone mode {mode}!')
            mode = globals()[f'CS_MODE_{mode}']
            cs = Cs(arch, mode)
            res = []
            for opc in cs.disasm(input, virt_addr):
                res.append(f'{opc.address:06X}:  {to_hex(opc.bytes):30} {opc.mnemonic} {opc.op_str}')
            return '\n'.join(res)
