"""
YARA rule support.

Author: Vlad Topan (vtopan/gmail)
"""
from anto.api import CFG, AnDataOp, log, err, find_files, eval_path, run, make_internal_link, dbg

import glob
import html
import os
import re
import tempfile


def compile_yara():
    """
    Compile YARA rules in `CFG.path.yara_rule_path` (default `$proj/anto/external/yara-rules`)
    and paths in `CFG.path.extra_yara_rule_paths`, if any.
    """
    paths = [CFG.path.get('yara_rule_path', eval_path('$proj/anto/external/yara-rules'))] + (CFG.path.extra_yara_rule_paths or [])
    for path in paths:
        rule_files = {}
        for f in find_files(path, ext=('yar', 'yara')):
            cat = f.replace(path, '').replace('\\', '/').strip('/').split('/')[0]
            if cat not in rule_files:
                rule_files[cat] = []
            rule_files[cat].append(f)
    old_list = set(glob.glob(CFG.path.yara_compiled_rules % '*'))
    for cat, files in rule_files.items():
        crf = CFG.path.yara_compiled_rules % cat
        last_update = max(max(os.path.getctime(f), os.path.getmtime(f)) for f in files)
        exists = os.path.isfile(crf) and max(os.path.getmtime(crf), os.path.getctime(crf))
        if (not exists) or (last_update > exists):
            log(f'{"Reg" if exists else "G"}enerating {crf}...')
            # run([eval_path('$ext_bin/yarac64.exe')] + [f'{cat}:{f}' for f in files] + [crf], check=True)
            p = run([eval_path('yarac')] + files + [crf])
            if p.returncode:
                err(f'Failed generating {crf}!')
        old_list.discard(crf)
    for f in old_list:
        os.remove(f)


def run_yara(target):
    """
    Run YARA on the given target (`bytes` instance or filename).
    """
    if not os.path.isfile(CFG.path.yara_compiled_rules):
        compile_yara()
    if isinstance(target, str):
        filename = target
    else:
        fd, filename = tempfile.mkstemp()
        os.close(fd)
        open(filename, 'wb').write(target)
    res = []
    for rule_file in glob.glob(CFG.path.yara_compiled_rules % '*'):
        dbg(f'Running YARA on {filename} with {rule_file}...')
        p = run(['yara', '-e', '-s', '-g', '-C', rule_file, filename])
        if p.returncode:
            err('Failed running YARA!')
        result = p.stdout.strip().decode('utf8', errors='replace')
        if result:
            res.append(f'RULE {os.path.basename(rule_file)}:\n{result}\n')
    if not isinstance(target, str):
        os.remove(filename)
    return ''.join(res)


def format_yara_results(yara_output, buffer_id=None):
    """
    Format YARA results as internal HTML.
    """

    def _callback(m):
        """
        re.sub() callback which converts YARA results in internal jump links.
        """
        grp = m.groups()
        if grp[1]:
            # hex pattern
            count = len(grp[1].replace(' ', '').strip()) // 2
        else:
            count = len(grp[2].strip())
        offset = int(m.groups()[0], 16)
        # todo: API for generating jump links
        pfx, rest = m.group().split(':', 1)
        return f'{make_internal_link(pfx, buffer_id, offset, count=count)}:{html.escape(rest)}'

    result = html.escape(yara_output)
    if buffer_id:
        result = re.sub(r'^0x([A-F0-9]+):.+?: (?:([A-F0-9]{2}(?: [A-F0-9]{2}){2,})[ \r]*$|(.+))',
                _callback, result, flags=re.M|re.I)
    result = re.sub(r'^RULE (.+):', r'<font color=#676>RULE</font> <font color=#808>\1</font>:', result, flags=re.M)
    result = re.sub(r'^default:(\w+)(.+)', r'<font color=#800>\1</font><font color=#676>\2</font>', result, flags=re.M)
    result = result.replace('\r', '').replace('\n', '<br>')
    return result



class An_Run_YARA(AnDataOp):
    HTML = True
    FILEOP = True


    def op(self, input, source):
        """
        The data operation.
        """
        self.buffer_id = None
        if not input:
            dv = self.api.win.dataview
            if not dv:
                return err('No open file!')
            input = dv.filename if dv.filename and os.path.isfile(dv.filename) else dv.raw
            if not input:
                return ''
            self.buffer_id = self.api.objmgr.gui_buffer_id
        result = run_yara(input)
        result = format_yara_results(result, self.buffer_id)
        return result


def init():
    if not 'yara_compiled_rules' in CFG.path:
        CFG.path.yara_compiled_rules = eval_path('$proj/anto/external/yara-rules/anto-yara-%s.yar.compiled')
    if not 'extra_yara_rule_paths' in CFG.path:
        CFG.path.extra_yara_rule_paths = []


init()
