"""
Extract strings from binary files.
"""
import copy
import re

from anto.api import CFG, AnDataOp, log, err, find_files, eval_path, run, make_internal_link, \
        data as iodata, AttrDict
from iolib.data import ellipsis


RX_PAT = {
    'ascii': b'[\\x20-\\x7E]{4,}',
    'utf16': b'(?:[\\x20-\\x7E]\\0){4,}',
    'utf8_special': b'(?:[\xE1\xE2][\x80-\x9F\xB0-\xBF\xAC]{2}|[\xCA\xCB][\xA0-\xBF]){4,}',
}
RX = {
    'ctf-interesting': ('f[1l][@a]g|g[1l][@a]f|[a-z]{2}://|pass', re.I),
}
for k in RX:
    RX[k] = re.compile(*RX[k])


def extract_strings(input, user_regex=None, min_len=6, utf16=True, max_count=10):
    """
    Extract strings from binary buffer.
    """
    pat = RX_PAT['ascii'] + b'|' + RX_PAT['utf8_special']
    if utf16:
        pat += b'|' + RX_PAT['utf16']
    pat = pat.replace(b'$min_len', str(min_len).encode())
    res = AttrDict(other={})
    rx = copy.copy(RX)
    if user_regex:
        rx['user'] = user_regex
    for m in re.finditer(pat, input):
        raw_s = m.group()
        s = raw_s.decode('utf16') if raw_s[1] == 0 else raw_s.decode('utf8')  #(s.decode('utf8') if s[0] in (0xE1, 0xE2) else s.decode())
        key = (m.start(), m.end() - m.start())
        for k in rx:
            if rx[k].search(s):
                if k not in res:
                    res[k] = {}
                if s not in res[k]:
                    res[k][s] = [key]
                elif len(res[k][s]) < max_count:
                    res[k][s].append(key)
                break
        else:
            if raw_s[0] in (0xE1, 0xE2, 0xCA, 0xCB):
                k = 'utf8-interesting'
                if k not in res:
                    res[k] = {}
                if s not in res[k]:
                    res[k][s] = [key]
                else:
                    res[k][s].append(key)
                continue
            if len(s) < min_len:
                continue
            if s not in res['other']:
                res['other'][s] = [key]
            elif len(res['other'][s]) < 10:
                res['other'][s].append(key)
    return res


def format_extracted_strings(strings, buffer_id='*'):
    """
    Format extracted strings as HTML.
    """
    res = []
    score = {'user': 2}
    for k in sorted(strings, key=lambda x:score.get(x, 1 if 'interesting' in x else 0), reverse=True):
        res.append(f'<h3>{k}</h3>')
        for s, ps in strings[k].items():
            res.append(ellipsis(s.replace("<", "&lt;"), 50) + ' @ ' + \
                    ', '.join(make_internal_link(f'0x{p:X}', buffer_id, offset=p, count=sz) for p, sz in ps))
    return '\n<br>'.join(res)



class An_Extract_strings(AnDataOp):
    HTML = True
    FILEOP = True


    def op(self, input, source):
        """
        The data operation.
        """
        self.buffer_id = None
        if not input:
            dv = self.api.win.dataview
            if not dv:
                return err('No open file!')
            input = dv.raw
            if not input:
                return
            self.buffer_id = self.api.objmgr.gui_buffer_id
        result = extract_strings(input)
        result = format_extracted_strings(result, self.buffer_id or '*')
        return result
