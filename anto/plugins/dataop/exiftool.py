import platform
import subprocess

from anto.api import CFG, AnDataOp, log, err, find_files, eval_path, run, make_internal_link, runner


def run_exiftool(what):
    """
    Get information about a file using `exiftool`.
    """
    available = runner.check_availability('exiftool')
    if not available:
        return err(f'Command `exiftool` not found!')
    cmd = ['exiftool']
    cmd_input = None
    if isinstance(what, str):
        cmd.append(what)
    else:
        # todo: this doesn't seem to work on Windows, always use filename
        cmd_input = what
    result = runner.run(cmd, input=cmd_input)
    return result.stdout.decode()



class An_Run_exiftool(AnDataOp):
    HTML = True
    ARGS = {
        }

    def op(self, input, source):
        """
        The data operation.

        :param source: The source DataView for the data (if known).
        """
        if not input:
            dv = self.api.win.dataview
            input = dv.filename if dv.filename and os.path.isfile(dv.filename) else dv.raw
            if not input:
                return ''
            self.buffer_id = self.api.objmgr.gui_buffer_id
        result = run_exiftool(input).replace(b'\x0A', b'<br>')
        return result
