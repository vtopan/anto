"""
Carve files and data for embedded files.

Author: Vlad Topan (vtopan/gmail)
"""
from anto.api import CFG, AnDataOp, log, err, dbg, find_files, eval_path, run, make_internal_link, runner
from iolib.net import PCap, PROTOCOLS
from iolib import AttrDict

import glob
import html
import io
import os
import re
import tempfile
import zipfile


def run_tcpdump(what, args):
    """
    Process file using tcpdump.
    """
    available = runner.check_availability('tcpdump')
    if not available:
        return err(f'Command `tcpdump` not available!')
    remotely = available == 'remotely'
    fid = None
    if isinstance(what, str):
        filename = what
    else:
        fid, filename = tempfile.mkstemp()
        dbg(f'Using temporary file {filename}...')
        os.close(fid)
        open(filename, 'wb').write(what)
    cmd = ['tcpdump', '-r', filename] + args
    tempdir = tempfile.gettempdir()
    p = runner.run(cmd, remotely=remotely, arg_file=filename, cwd=tempdir)
    if fid is not None:
        os.remove(filename)
    result = (p.stdout + p.stderr).decode()
    return result


def extract_streams_from_pcap(data):
    """
    Extract data streams from a PCAP.
    """
    res = {}
    pcap = data if isinstance(data, PCap) else PCap(data)
    for i, pkt in enumerate(pcap.packets):
        l1proto = PROTOCOLS.get(pkt.proto)
        if l2 := pkt.next:
            l2proto = PROTOCOLS.get(l2.proto)
            if l3 := l2.next:
                l3proto = PROTOCOLS.get(l3.proto)
                if len(l3) != l3.hsize:
                    key = f'{l3proto[0]}-{l2.src}-{l3.src}-{l2.dst}-{l3.dst}'
                    if key not in res:
                        res[key] = []
                    res[key].append(l3.data[l3.hsize:])
    for key in res:
        res[key] = d = AttrDict(data=b''.join(res[key]), name=key)
        d.proto, d.src_addr, d.src_port, d.dst_addr, d.dst_port = key.split('-')
        d.src_port, d.dst_port = int(d.src_port), int(d.dst_port)
    return res



class An_Convert_PCAPNG_To_PCAP(AnDataOp):
    FILTER = b'^0\x0A\x0D\x0D\x0A'

    def op(self, input, source):
        dv = self.api.objmgr.gui_dataview()
        parent = self.api.dataview(dv.buffer_id)
        if not input:
            if not dv:
                return err('No open file!')
            parent = dv.parent
            input = dv.filename if dv.filename and os.path.isfile(dv.filename) else dv.raw
            if not input:
                return ''
        out = f'output.pcapng'
        result = run_tcpdump(input, args=['-w', out])
        if result is not None:
            available = runner.check_availability('tcpdump')
            if available == 'remotely':
                zdata = runner.gather_remote_files('/tmp/' + out)   # /tmp/ = likely assumption
                zf = zipfile.ZipFile(io.BytesIO(zdata))
                data = zf.read(os.path.basename(out))
                self.api.load(parent=parent, filename=f'converted.pcap',
                        data=data, virtual=True, relation='convert')
            else:
                self.api.load(parent=parent, filename=out, relation='convert')

