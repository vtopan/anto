import platform
import re
import subprocess

from anto.api import CFG, AnDataOp, log, err, find_files, eval_path, run, make_internal_link, check_availability


FILE_BRIEF = {
    'data': 'binary',
}


def run_file(what, keep_going=False, brief=False):
    """
    Identify a file or buffer using the `file` utility.

    :param what: Filename (str) or buffer (bytes).
    """
    available = check_availability('file')
    if not available:
        return err(f'Command `file` not available!')
    cmd = ['file', '-b']
    if keep_going:
        cmd.append('-k')
    cmd_input = None
    if isinstance(what, str):
        cmd.append(what)
    else:
        cmd_input = what
        cmd.append('-')
    result = run(cmd, input=cmd_input).stdout.strip()\
            .replace(b'\\012- ', b'\n').replace(b'\ndata', b'').decode()
    if brief:
        result = brief_file(result)
    return result


def brief_file(text):
    """
    Shorten (preferably to the extension) the `file` command output.
    """
    # result = re.sub(' (?:archive|image) data$| (?:document )?text$', '', text.split(',')[0])
    result = text.split(' ')[0]
    result = FILE_BRIEF.get(result, result)
    return result



class An_Run_file(AnDataOp):
    HTML = True
    ARGS = {
            'keepgoing': ('Keep going', False),
        }
    # todo: implement ARGS support in GUI

    def op(self, input, source):
        """
        The data operation.

        :param source: The source DataView for the data (if known).
        """
        if not input:
            dv = self.api.win.dataview
            if not dv:
                return err('No open file!')
            input = dv.filename if dv.filename and os.path.isfile(dv.filename) else dv.raw
            if not input:
                return ''
        result = run_file(input).replace(b'\x0A', b'<br>')
        return result

