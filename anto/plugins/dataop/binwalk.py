import glob
import io
import os
import platform
import subprocess
import tempfile
import zipfile

from anto.api import CFG, AnDataOp, log, err, dbg, find_files, eval_path, run, make_internal_link, runner


def run_binwalk(what):
    """
    Extract embedded data using binwalk.
    """
    available = runner.check_availability('binwalk')
    if not available:
        return err(f'Command `binwalk` not available!'), None
    remotely = available == 'remotely'
    fid = None
    if isinstance(what, str):
        filename = what
    else:
        fid, filename = tempfile.mkstemp()
        dbg(f'Using temporary file {filename}...')
        os.close(fid)
        open(filename, 'wb').write(what)
    cmd = ['binwalk', '-D=.*', '-o=1', filename]
    tempdir = tempfile.gettempdir()
    p = runner.run(cmd, remotely=remotely, arg_file=filename, cwd=tempdir)
    if fid is not None:
        os.remove(filename)
    # gather extracted files
    if remotely:
        glob_pat = f'{os.path.dirname(p.local_fn)}/_{os.path.basename(p.local_fn)}.extracted/*'
        zdata_or_lst = runner.gather_remote_files(glob_pat)
    else:
        glob_pat = f'{tempdir}/_{os.path.basename(filename)}.extracted/*'
        zdata_or_lst = list(glob.glob(glob_pat))
    result = (p.stdout + p.stderr).decode()
    return result, zdata_or_lst



class An_Run_binwalk(AnDataOp):
    HTML = True
    ARGS = {
        }

    def op(self, input, source):
        """
        The data operation.

        :param source: The source DataView for the data (if known).
        """
        if not input:
            dv = self.api.objmgr.gui_dataview()
            if not dv:
                return err('No open file!')
            input = dv.filename if dv.filename and os.path.isfile(dv.filename) else dv.raw
            if not input:
                return ''
        result, zdata = run_binwalk(input)
        parent=getattr(source, 'buffer_id', None)
        if parent:
            parent = self.api.dataview(parent)
        if zdata:
            if isinstance(zdata, list):
                for fn in zdata:
                    self.api.load(parent=parent, filename=fn, relation='embedded')
            else:
                bio = io.BytesIO(zdata)
                zf = zipfile.ZipFile(bio)
                for fn in zf.namelist():
                    data = zf.read(fn)
                    self.api.load(parent=parent, filename=f'embedded@0x{int(fn):X}.bin',
                            data=data, virtual=True, relation='embedded')
        if isinstance(result, str):
            result = result.replace('\x0A', '<br>')
        return result
