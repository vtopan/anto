"""
Carve files and data for embedded files.

Author: Vlad Topan (vtopan/gmail)
"""
from anto.api import CFG, AnDataOp, log, err, find_files, eval_path, run, make_internal_link, data as iodata

import glob
import html
import os
import re
import tempfile


class An_Find_Embedded_Files(AnDataOp):
    HTML = True
    FILEOP = True

    def op(self, input, source):
        """
        The data operation.

        :param source: The source DataView for the data (if known).
        """
        result = []
        filename = 'buffer'
        self.buffer_id = self.api.objmgr.gui_buffer_id
        if self.buffer_id < 0:
            return
        parent = self.api.dataview(self.buffer_id)
        if not input:
            dv = self.api.win.dataview
            if not dv:
                return err('No open file!')
            input, filename = dv.raw, dv.filename
        for i, (offset, size, ext) in enumerate(iodata.extract_embedded_files(input)):
            line = make_internal_link(ext + " @ " + hex(offset), self.buffer_id or '*', offset, count=size) + f' ({size} bytes)'
            result.append(line)
            data = input[offset:offset + size]
            self.api.load(parent, f'{filename}.embedded#{i:03}.{ext}', data=data, relation='embedded', virtual=True)
        result = '<br>'.join(result)
        return result
