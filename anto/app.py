"""
AnTo: (GUI) application.

Author: Vlad Topan (vtopan/gmail)
"""
from .ui import run_ui
from .api import AnApi, save_config

import sys


class GuiApp:
    """
    Glue code: AnTo GUI application.
    """


    def __init__(self, filename=None):
        self.api = AnApi(main_window=run_ui())
        if filename:
            self.api.load(None, filename)


    def run_forever(self):
        res = self.api.win.app.exec_()
        save_config()
        sys.exit(res)
