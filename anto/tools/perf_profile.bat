@echo off
set proj=%~dp0..\..
for %%f in ("%proj%") do set proj=%%~ff
set perf_log=%proj%\perf.profile
if _%1_ == _run_ python.exe -m cProfile -o %perf_log% -s tottime %proj%\anto.py
if exist %perf_log% python.exe -c "import pstats; p = pstats.Stats(r'%perf_log%'); p.sort_stats(pstats.SortKey.CUMULATIVE).print_stats(100)" > %perf_log%.log
