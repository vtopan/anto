#!/usr/bin/env python3

from iolib import AttrDict

import base64
import hashlib
import json
import shlex
import socket
import struct
import subprocess
import traceback
import zlib

from anto.api.lib.net import send_packed_data, recv_packed_data, recvall, recv_int, send_int


def sign_message(msg, key):
    """
    Sign a message.
    """
    if isinstance(key, str):
        key = key.encode()
    sig = hashlib.sha512(msg + key).digest()
    return sig


def run_remotely(command, rce_host, stdin=None):
    # todo: update to new protocol
    p = AttrDict(returncode=-1, stdout=b'', stderr=b'')
    try:
        cmd = json.dumps(command).encode()
        stdin = stdin or b''
        sig = sign_message(cmd + stdin, rce_host.key)
        cmd = b'x' + struct.pack('<HHI', len(sig), len(cmd), len(stdin)) + sig + cmd + stdin
        with socket.socket(socket.AF_INET6 if ':' in rce_host.host else socket.AF_INET,
                socket.SOCK_STREAM) as s:
            s.connect((rce_host.host, rce_host.port))
            print('>', cmd)
            s.sendall(cmd)
            res = s.recv(1)
            print('<', res)
            if res != b'k':
                print(f'RCE protocol error: {res}!')
            else:
                buf = s.recv(12)
                print('<', buf)
                p.returncode, out_size, err_size = struct.unpack('<iII', buf)
                received, chunks = 0, []
                while received < out_size + err_size:
                    chunks.append(s.recv(out_size + err_size - received))
                    print('<', chunks[-1])
                    received += len(chunks[-1])
                output = b''.join(chunks)
                p.stdout, p.stderr = output[:out_size], output[out_size:]
    except Exception as e:
        print(f'Failed running command {command} remotely: {e}')
        traceback.print_exc(3)
    return p


key = input('Key: ')
rh = AttrDict(host='localhost', port=65432, key=key.encode())
print(run_remotely(['cat'], rh, b'hello world!'))
