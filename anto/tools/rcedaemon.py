#!/usr/bin/env python
"""
anto remote command execution server.

Author: Vlad Topan (vtopan/gmail)
"""
import argparse
import base64
import glob
import hashlib
import json
import os
import platform
import random
import shlex
import socket
import io
import struct
import subprocess
import sys
import tempfile
import threading
import time
import zipfile
import zlib


VER = '0.1.0.20230925'
KEY = None
HOST = 'localhost'
PORT = 65432
THREADS = []
TMPFILES = []



class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__dict__ = self



def recv_int(s):
    """
    Recieve an int from a socket.
    """
    return struct.unpack('<i', s.recv(4))


def send_int(s, value):
    """
    Recieve an int from a socket.
    """
    return s.send(struct.pack('<i', value))


def send_packed_data(s, *args):
    """
    Pack data (prefix with size) for sending over socket.
    """
    for data in args:
        if isinstance(data, int):
            send_int(s, data)
            continue
        if isinstance(data, str):
            data = data.encode()
        s.sendall(struct.pack('<i', len(data)) + data)


def recv_packed_data(s, count=1):
    """
    Receive packed data from socket.
    """
    res = []
    for i in range(count):
        size = struct.unpack('<i', s.recv(4))[0]
        res.append(recvall(s, size))
    return res[0] if count == 1 else res


def recvall(s, size):
    """
    Recieve a number of bytes.
    """
    received, chunks = 0, []
    while received < size:
        chunks.append(s.recv(size - received))
        received += len(chunks[-1])
    return b''.join(chunks)


def check_sig(msg, sig):
    """
    Check a signature on a message.
    """
    if CFG.no_sig_check:
        return True
    ok_sig = hashlib.sha512(msg + KEY).digest()
    return sig == ok_sig


def generate_key():
    """
    Generate signing key.
    """
    key = bytes(random.randint(0, 255) for i in range(20)).hex()
    return key.encode()


def run_command(cmd, input=None):
    """
    Execute a shell command and collect the output.
    """
    p = AttrDict(returncode=-1, stdout=b'', stderr=b'')
    try:
        input = input or None
        shell = True
        cwd = None
        if platform.system() == 'Linux':
            shell = False
            cwd = '/tmp'
        print(f'[*] Running command (stdin={len(input or "")} bytes): {cmd}')
        p = subprocess.run(cmd, capture_output=True, input=input, shell=shell, cwd=cwd)
    except FileNotFoundError as e:
        print(f'[!] Failed running {cmd}: {e}')
        p.returncode = -2
    except Exception as e:
        print(f'[!] Failed running {cmd}: {e}')
    return p


def create_file(data):
    """
    Create a (temporary) file.
    """
    fid, filename = tempfile.mkstemp()
    os.close(fid)
    TMPFILES.append(filename)
    open(filename, 'wb').write(data)
    return filename


def handle_client(s, remote_addr):
    """
    Handle client.
    """
    with s:
        print(f'[*] {remote_addr} connected.')
        while True:
            cmd = s.recv(1)
            if not cmd:
                continue
            if cmd[0] == ord('q'):
                break
            elif cmd[0] in (ord('f'), ord('F')):
                # read file(s)
                sig, filename = recv_packed_data(s, count=2)
                filename = filename.decode()
                print(f'[*] Looking for {filename}...')
                if not check_sig(filename, sig):
                    print(f'[!] Invalid signature for filename {filename}!')
                    s.sendall(b'i')
                    break
                bio = io.BytesIO()
                remove = cmd[0] == ord('F')
                with zipfile.ZipFile(bio, mode='w', compression=zipfile.ZIP_DEFLATED) as zf:
                    for f in glob.glob(filename):
                        print(f'[-] Found {f} (remove:{remove})...')
                        zf.write(f, os.path.basename(f))
                        if remove:
                            os.remove(f)
                data = bio.getvalue()
                s.send(b'k')
                print(f'[*] Sending {len(data)} bytes...')
                send_packed_data(s, data)
            elif cmd[0] == ord('x'):
                local_fn = ''
                sig, command, input, filename, filedata = recv_packed_data(s, count=5)
                if not check_sig(command + input + filename, sig):
                    print(f'[!] Invalid signature for command {command}!')
                    s.sendall(b'i')
                    break
                command = json.loads(command)
                if filedata:
                    local_fn = create_file(filedata)
                    if filename:
                        filename = filename.decode()
                        if isinstance(command, str):
                            command = command.replace(filename, local_fn)
                        else:
                            command = [e.replace(filename, local_fn) for e in command]
                p = run_command(command, input=input)
                if local_fn:
                    os.remove(local_fn)
                s.send(b'k')
                print(f'[*] Sending process output ({len(p.stdout)}+{len(p.stderr)})...')
                send_packed_data(s, p.returncode, local_fn, p.stdout, p.stderr)
            else:
                s.sendall(b'e')
                print(f'[!] Invalid / unknown command: {cmd}!')
                break
    THREADS.remove(threading.current_thread())


def listen():
    """
    Listen for commands.
    """
    with socket.socket(socket.AF_INET6 if ':' in CFG.host else socket.AF_INET, socket.SOCK_STREAM) as s:
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((CFG.host, CFG.port))
        print(f'[*] Listening on {CFG.host}:{CFG.port}...')
        s.listen()
        try:
            while 1:
                conn, addr = s.accept()
                t = threading.Thread(target=handle_client, args=(conn, addr))
                t.start()
                THREADS.append(t)
        except KeyboardInterrupt:
            print('[*] Shutting down...')
            for f in TMPFILES:
                try:
                    os.remove(f)
                except:
                    pass
            for t in THREADS:
                t.join()



class TeeOutput:

    def write(self, s):
        sys.__stdout__.write(s)
        open(CFG.logfile, 'a').write(s)

    def flush(self):
        sys.__stdout__.flush()


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--port', help='Listening port', default=65432,
        type=int)
parser.add_argument('-H', '--host', help='Listening host', default='0.0.0.0')
parser.add_argument('-l', '--logfile', help='Log file', default='rcedaemon.log')
parser.add_argument('-nl', '--no-log', help='Do NOT log output to the log file',
        action='store_true')
parser.add_argument('-ns', '--no-sig-check', help='Do NOT check command signatures (DANGEROUS!)',
        action='store_true')
CFG = parser.parse_args()

if not CFG.no_log:
    sys.stdout = TeeOutput()
print(f'[*] anto RCEDaemon {VER} started @ {time.strftime("%H:%M:%S, %d.%m.%Y")}')
KEY = generate_key()
print(f'[*] Key: {"NO-SIG-CHECK" if CFG.no_sig_check else KEY.decode()}')
listen()
