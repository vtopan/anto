"""
parsec: Parse C source to extract typedefs & autogenerate parser plugin components.

Author: Vlad Topan (vtopan/gmail)
"""

import os
import sys

sys.path.append(os.path.abspath((os.path.dirname(__file__) or '.') + '/../..'))

from anto.api import parse_c_to_anstruct

filename = sys.argv[1]
data = open(filename).read()
print(parse_c_to_anstruct(data))
