"""
fdump: Parse and dump files using the same dissectors as AnTo.

Author: Vlad Topan (vtopan/gmail)
"""
import argparse
import ast
import os
import pprint
import re
import struct
import sys

sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/../..'))

from anto.api import lib

RX = {
    }
for k in RX:
    RX[k] = re.compile(*RX[k])


def msg(s):
    sys.stderr.write(str(s) + '\n')


def dump_file(filename):
    """
    Attempt to guess the file type and dump the file.
    """
    ext = None
    hdr = open(filename, 'rb').read(4096)
    if '.' in filename:
        basename, ext = filename.rsplit('.', 1)
    res = None
    if ext in ('cer', 'p7b') or hdr.startswith(b'-----BEGIN CERT'):
        res = lib.parse_certificate_file(filename)
    elif ext == 'cat' or (hdr[0] == b'0' and (hdr[1] & 0xF0 == 0x80)):
        embedded = [] if CFG.embedded else None
        res = lib.parse_catalog(filename, embedded=embedded)
        if CFG.embedded:
            for i, e in enumerate(embedded):
                fn = f'{basename}-cert#{(i + 1):02}.cer'
                msg(f'[*] Writing to {fn}...')
                open(fn, 'wb').write(e)
    if res:
        msg(f'[*] File {filename}:')
        print(lib.dump_dict(None, res))


argp = argparse.ArgumentParser()
argp.add_argument('targets', help='file name(s)', nargs='*')
# argp.add_argument('-r', '--recursive', help='process targets (folders) recursively', action='store_true')
argp.add_argument('-e', '--embedded', help='extract embedded files / buffers', action='store_true')
CFG = argp.parse_args()

for f in CFG.targets:
    dump_file(f)
