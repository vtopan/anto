#!/usr/bin/env python3
import os
import platform
import sys

PROJ_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(PROJ_PATH)
os.environ['PATH'] += f'{os.pathsep}{PROJ_PATH}/anto/pubtools/{platform.system()}'

from anto import GuiApp


GuiApp(sys.argv[1] if len(sys.argv) > 1 else None).run_forever()

