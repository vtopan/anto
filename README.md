# AnTo: Analysis Toolkit - pre-release preview

AnTo is (intended to be) an analysis toolkit for malware samples & APTs combined with a document
(reference) storage geared towards easy & fast information retrieval.

![anto 0.1.0 screenshot](docs/anto-0.1.0.png)


## Manual

- Alt + left / right click for a continuous selection (without Alt a block is selected)


## Plugins


### YARA

- scanning with YARA rules is provided by `plugins/dataop/yara.py` for buffers and files
    - add extra rules to `anto/external/yara-rules` (in a subfolder) or configure additional
        search paths in `anto.cfg` by setting `{'path': {'extra_yara_rule_paths': [...] } }`


## Third Party Components

- uses the excellent DOS font *Px437 IBM VGA9* from <https://int10h.org/oldschool-pc-fonts/> (CC-BY-SA-4.0)
